import inspect
import os
import warnings

import numpy as np


class Molecule:
    """Hygo molecule class.

    Parameters
    ----------
    molecule : :obj:`str` or :obj:`daltonproject.Molecule`
        file name or string containing coordinates or daltonproject Molecule instance

    """

    def __init__(self, molecule):
        # todo : add zmat and pdb functionalities from HSP here
        self.atoms = []
        if isinstance(molecule, str):
            if os.path.isfile(molecule):
                with open(molecule, 'r') as myfile:
                    lines = list(filter(None, myfile.read().split('\n')))
                try:
                    self.num_atoms = int(lines[0])
                except Exception:
                    raise AttributeError(f'file {molecule} not a proper xyz-file')
                self.coordinates = []
                for line in lines[2:self.num_atoms + 2]:
                    split_line = line.split()
                    self.coordinates.append([float(value) for value in split_line[1:4]])
                    self.atoms.append(str(split_line[0]))

                self.coordinates = np.array(self.coordinates)

            else:
                lines = list(molecule.strip().split('\n'))
                # a single string, i.e. number of atoms
                if lines[0].count(' ') == 0:
                    self.num_atoms = int(lines.pop(0))
                    # remove comment line too
                    lines.pop(0)
                    # print(lines)

                    if len(lines) < self.num_atoms:
                        raise Exception('number of atoms not correct')
                    elif len(lines) > self.num_atoms:
                        warnings.warn(f'only {self.num_atoms} out of {len(lines)} coordinates read.')

                else:
                    self.num_atoms = -1
                self.coordinates = np.zeros((len(lines), 3))
                for i, line in enumerate(lines):
                    line = list(filter(None, line.strip().split(' ')))
                    atom, *xyz = line
                    if len(xyz) != 3:
                        raise Exception('only xyz supported')
                    self.atoms.append(atom)
                    self.coordinates[i] = np.array(list(map(float, xyz)), dtype=float)

                if self.num_atoms == -1:
                    self.num_atoms = len(self.atoms)

            self.properties = {}

        else:

            if inspect.isclass(type(molecule)):
                # this should be the standard case
                # support for hylleraas and daltonproject
                try:
                    try:
                        self.atoms = molecule.elements
                        self.properties = {'charge': molecule.charge, 'point_group': molecule.point_group}
                    except Exception:
                        self.atoms = molecule.atoms
                        self.properties = molecule.properties
                    finally:
                        self.coordinates = molecule.coordinates
                        self.num_atoms = len(self.atoms)
                except Exception:
                    raise Exception(f'could not read molecule from object {molecule} with type {type(molecule)}')

        if self.atoms == []:
            raise Exception('unknown molecule type')
        else:
            self.atoms = [atom.capitalize() for atom in self.atoms]

    def __repr__(self):  # pragma no cover
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))
