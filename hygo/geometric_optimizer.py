# import os
import tempfile
import warnings
from pathlib import Path
from typing import Any, Dict, List

import numpy as np

from .check import check_iteration
from .method import Compute
from .molecule import Molecule

try:
    from qcelemental import PhysicalConstantsContext
    constants = PhysicalConstantsContext('CODATA2018')
    bohr2angstrom = constants.bohr2angstroms
except ImportError:  # pragma no cover (should be installed together with daltonproject)
    bohr2angstrom = 0.529177210903
    warnings.warn('qcelemental failed to import, continuing with old data', ImportWarning)

try:
    import geometric
    import geometric.molecule
except ImportError:  # pragma no cover (should be installed)
    warnings.warn('geometric failed to import', ImportWarning)


class GeometricOptimizer:
    """Hygo optimizer engine based on geomeTRIC.

    Parameters
    ----------
    molecule : :obj:`Molecule`
        hygo molecule instance
    method : :obj:`Compute`
        hygo compute instance
    options : :obj:`dict`
        dictionary containing options for (in this case geometric)

    """

    def __init__(self, molecule: Molecule, method: Compute, options: Dict):
        geometric_molecule = geometric.molecule.Molecule()
        geometric_molecule.elem = molecule.atoms
        geometric_molecule.xyzs = [molecule.coordinates * bohr2angstrom]
        engine = CustomEngine(geometric_molecule, molecule, method, options)
        geometric_options = self.set_geometric_kwargs(options)
        tmpf = tempfile.mktemp()

        if options['print_level'] > 0:
            str1 = ' it'
            str2 = '      energy'
            str3 = '      ||gradient||'
            print(f'{str1:3}\t{str2:20}\t{str3:20}')
            line_str = '-' * 80
            print(line_str)

        geoopt = geometric.optimize.run_optimizer(customengine=engine, check=1, input=tmpf, **geometric_options)

        self.summary: Dict = {}
        self.summary['energies'] = geoopt.qm_energies
        self.summary['gradients'] = geoopt.qm_grads
        self.summary['geometries'] = geoopt.xyzs
        self.summary['final_energy'] = geoopt.qm_energies[-1]
        self.summary['final_geometry_angstrom'] = geoopt.xyzs[-1]
        self.summary['final_geometry_bohr'] = geoopt.xyzs[-1] / bohr2angstrom
        self.summary['atoms'] = molecule.atoms

        gradnorm = np.linalg.norm(self.summary['gradients'][-1].ravel()) / np.sqrt(len(molecule.atoms))
        if len(self.summary['energies']) > 1:
            endiff = np.abs(self.summary['energies'][-1] - self.summary['energies'][-2])
        iconv = 0
        iconv = iconv + \
            1 if gradnorm < options['convergence_gradient_max'] else iconv
        iconv = iconv + 1 if endiff < options['convergence_energy'] else iconv
        self.summary['is_converged'] = True if iconv == 2 else False

    @staticmethod
    def set_geometric_kwargs(options: Dict) -> Dict:
        """Parser for geomeTRIC options.

        Parameters
        ----------
        options : :obj:`dict`
            options for the optimizer

        Returns
        -------
        :obj:`dict`
            dictionary parsed as ``**kwargs`` to geomeTRIC

        """
        kwargs: Dict[Any, Any] = {}
        logIni = """[loggers]
keys=root

[handlers]
keys=file_handler

[formatters]
keys=formatter

[logger_root]
level=INFO
handlers=file_handler

[handler_file_handler]
class=geometric.nifty.RawFileHandler
level=INFO
formatter=formatter
args=('./geomeTRIC.log',)

[formatter_formatter]
# format=%(message)s
format=%(asctime)s %(name)-12s %(levelname)-8s %(message)s
"""  # noqa: N806
        kwargs['logIni'] = options.get('logIni', 'log.ini')
        # kwargs['logIni'] = os.path.abspath(os.path.join(__file__, '..', 'log.ini'))

        inifile = Path(kwargs['logIni'])
        if not inifile.exists():
            try:
                write_ini = open(inifile, 'w')
                write_ini.write(logIni)
                write_ini.close()
            except Exception:
                raise Exception(f'could not find file {inifile}')

        if isinstance(options['constraints'], dict):
            kwargs['constraints'] = 'myconstraints.txt'
            myfile = open('myconstraints.txt', 'w')
            if 'freeze' in options['constraints'].keys():
                freezeopt = options['constraints']['freeze']
                freezeopt_mod = [str(int(f) + 1) for f in freezeopt[1:]]
                freezeopt_mod.insert(0, freezeopt[0])
                myfile.write('$freeze\n')
                # if freezeopt[-1] == 'xyz':
                #     temp = freezeopt[1:]
                #     temp.reverse()
                #     freezeopt = temp
                myfile.write(' '.join(map(str, freezeopt_mod)))
                myfile.write('\n')

            if 'set' in options['constraints'].keys():
                setopt = options['constraints']['set']
                setopt_mod = [setopt[0]]
                for i in range(1, len(setopt) - 1):
                    setopt_mod.append(str(int(setopt[i]) + 1))
                setopt_mod.append(setopt[-1])
                myfile.write('$set\n')
                myfile.write(' '.join(map(str, setopt_mod)))
                myfile.write('\n')

            if 'scan' in options['constraints'].keys():
                scanopt = options['constraints']['scan']
                scanopt_mod = [scanopt[0]]
                for i in range(1, len(scanopt) - 3):
                    scanopt_mod.append(str(int(scanopt[i]) + 1))
                scanopt_mod.append(scanopt[-3])
                scanopt_mod.append(scanopt[-2])
                scanopt_mod.append(scanopt[-1])

                myfile.write('$scan\n')
                myfile.write(' '.join(map(str, scanopt_mod)))
                myfile.write('\n')

            myfile.close()

        kwargs['convergence_energy'] = options['convergence_energy']
        kwargs['convergence_grms'] = options['convergence_gradient_rms']
        kwargs['convergence_gmax'] = options['convergence_gradient_max']
        kwargs['convergence_drms'] = options['convergence_step_rms']
        kwargs['convergence_dmax'] = options['convergence_step_max']

        if options['optimization_target'].lower() == 'transition_state':
            kwargs['transition'] = True
            # kwargs['trust'] = 0.1
            # kwargs['tmax'] = 0.3

        kwargs['coordsys'] = options['geometric_coordsys']
        if options['geometric_convergence_set'] is not None:
            kwargs['convergence_set'] = options['geometric_convergence_set']

        if options['debug']:
            print('\ngeometric kwargs : ', kwargs)

        return kwargs


class CustomEngine(geometric.engine.Engine):
    """Custom engine for geomeTRIC.

    Parameters
    ----------
    geometric_molecule : :obj:`Molecule`
        geometric molecule instance generated from :obj:`class  GeometricOptimizer`instance
    molecule : :obj:`Molecule`
        hygo Molecule instance
    method : :obj:`Method`
        hygo Compute instance
    options : :obj:`dict`
        dictionary containing options for (in this case geometric).
        May (Should) contain options parsed through set_geometric_kwargs()

    """

    def __init__(self, geometric_molecule, molecule: Molecule, method: Compute, options: Dict) -> None:
        super(CustomEngine, self).__init__(geometric_molecule)
        self.steps = 0
        self.molecule = molecule
        self.method = method
        self.options = options
        self.steps_to_check = self.options['convergence_check_steps']
        self.steps_to_check_start = self.options['convergence_check_start']
        self.energies: List = []
        self.gradients: List = []
        self.geometries: List = []
        self.run = True
        self.summary: dict = {}

    def calc_new(self, coordinates, dirname) -> Dict:
        """One Iteration: Computing, energy, gradient and convergence measures.

        Parameters
        ----------
        coordinates : :obj:`array`
            array with xyz coordinates in dimension *(natoms, 3)*
        dirname : :obj:`str`
            used internally in geoemtric engine

        Returns
        -------
        :obj:`dict`
            dictionary containing ``energy`` and ``gradient``

        """
        coordinates = coordinates.ravel().reshape(-1, 3)

        if not self.run:
            return {'energy': self.energies[-1], 'gradient': self.gradients[-1]}

        self.geometries.append(coordinates)
        self.summary['last_stable'] = -1
        if (self.steps >= self.steps_to_check and self.steps > self.steps_to_check_start - 1):
            measures = check_iteration(
                self.energies[-self.steps_to_check:],
                self.gradients[-self.steps_to_check:],
                self.geometries[-self.steps_to_check - 1:],
                self.options,
            )

            if measures['is_stable']:
                self.summary['last_stable'] = self.steps

            if (self.steps - self.summary['last_stable'] > self.options['convergence_unstable_steps_allowed']):
                self.summary['is_converged'] = False
                warnings.warn('instability detected')
                print('\n stability measures:')
                try:
                    import json
                    found_json = True
                except Exception:
                    found_json = False

                if found_json:
                    print(json.dumps(measures, sort_keys=False, indent=2))
                else:
                    print(measures)

                self.run = False

        # molecule_args = self.method.set_args(locals(), self.method.molecule_routine_args)
        # molecule_kwargs = self.method.set_args(locals(), self.method.molecule_routine_kwargs)
        # try:
        #     getattr(self.method.compute_class, self.method.molecule_routine)(*molecule_args, **molecule_kwargs)
        # except Exception:
        #     raise Exception('could not update molecule on interface side')
        # finally:
        self.molecule.coordinates = coordinates.ravel().reshape(-1, 3)
        molecule = self.molecule

        energy_args = self.method.set_args(locals(), self.method.energy_routine_args)
        energy_kwargs = self.method.set_args(locals(), self.method.energy_routine_kwargs)

        energy = getattr(self.method.compute_class, self.method.energy_routine)(*energy_args, **energy_kwargs)

        gradient_args = self.method.set_args(locals(), self.method.gradient_routine_args)
        gradient_kwargs = self.method.set_args(locals(), self.method.gradient_routine_kwargs)

        gradient = getattr(self.method.compute_class, self.method.gradient_routine)(*gradient_args,
                                                                                    **gradient_kwargs)
        gradient = gradient.ravel()

        gradnorm = np.linalg.norm(gradient) / np.sqrt(len(gradient) / 3)
        if len(self.energies) > 1:
            endiff = np.abs(self.energies[-1] - self.energies[-2])
        else:
            endiff = 1e6

        iconv = 0
        iconv = (iconv + 1 if gradnorm < self.options['convergence_gradient_max'] else iconv)
        iconv = iconv + \
            1 if endiff < self.options['convergence_energy'] else iconv

        if self.options['convergence_geometric_criterions'] is None:
            if iconv == 2:
                self.run = False
            else:
                self.run = True

        if self.options['print_level'] > 0:
            print(f'{self.steps:3d}\t{energy:20.14f}\t{np.linalg.norm(gradient):20.14f}')

        self.steps += 1
        if self.steps >= self.options['steps_max']:
            self.run = False
        self.energies.append(energy)
        self.gradients.append(gradient)

        return {'energy': energy, 'gradient': gradient}
