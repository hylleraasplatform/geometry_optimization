import copy
import warnings
from typing import Any, Dict, Optional

import numpy as np

from .check import check_iteration
from .geometric_optimizer import GeometricOptimizer
from .method import Compute
from .molecule import Molecule
from .optking_optimizer import OptkingOptimizer

# from .environment import Environment


def optimize(molecule: Molecule, method: Compute, options: Dict, compute_settings: Optional[Any] = None) -> Dict:
    """Hygo geometry optimization driver (inner layer).

    Parameters
    ----------
    molecule : :obj:`Molecule`
        hygo Molecule instance
    method : :obj:`Compute`
        hygo Compute instance
    options : :obj:`dict`, optional
        options for the optimizer
    compute_settings: Any
        computational environment

    Returns
    -------
    :obj:`dict`
        dictionary summarizing the geometry optimization

    """
    initial_geometry = molecule.coordinates
    result: Any = None

    if options['optimizer'].lower() == 'geometric':
        result = GeometricOptimizer(molecule, method, options)
    elif options['optimizer'].lower() == 'optking':
        result = OptkingOptimizer(molecule, method, options)  # pragma no cover (needs psi4/conda)
    elif options['optimizer'].lower() == 'steepest_descent':
        options.update({'hessian_update': None})
        result = HygoOptimizer(molecule, method, options)
    elif options['optimizer'].lower() == 'bfgs':
        options.update({'hessian_update': 'BFGS'})
        result = HygoOptimizer(molecule, method, options)
    elif options['optimizer'].lower() == 'modified_hessian':
        options.update({'hessian_update': 'full'})

        result = HygoOptimizer(molecule, method, options)

    # elif options['optimizer'].lower() == 'native':  # pragma no cover (to be tested by daltonproject community)

    #     if 'dalton' in method.program.lower():  # includes lsdalton
    #         if options['optimization_target'].lower() == 'minimum':
    #             prop = dp.Property(geometry_optimization=True)
    #         elif options['optimization_target'].lower() == 'transition_state':
    #             prop = dp.Property(geometry_optimization=True, transition_state=True)

    #         if method.program.lower() == 'lsdalton':
    #             native_result = dp.lsdalton.compute(
    #                 molecule=method.my_molecule,
    #                 basis=method.my_basis,
    #                 qc_method=method.my_method,
    #                 properties=prop,
    #             )

    #         elif method.program.lower() == 'dalton':
    #             native_result = dp.dalton.compute(
    #                 molecule=method.my_molecule,
    #                 basis=method.my_basis,
    #                 qc_method=method.my_method,
    #                 properties=prop,
    #             )

    #         result.summary = {}
    #         if hasattr(native_result, 'geoopt_summary'):
    #             result.summary['energies'] = native_result.geoopt_summary['energies']
    #             gradnorm = native_result.geoopt_summary['gradient_norms'][-1]
    #             endiff = np.abs(native_result.geoopt_summary['energies'][-1]
    #                             - native_result.geoopt_summary['energies'][-2])
    #             iconv = 0
    #             iconv = (iconv + 1 if gradnorm < options['convergence_gradient_max'] else iconv)
    #             iconv = iconv + \
    #                 1 if endiff < options['convergence_energy'] else iconv
    #             result.summary['is_converged'] = True if iconv == 2 else False
    #             if options['print_level'] > 0:
    #                 str1 = ' it'
    #                 str2 = '      energy'
    #                 str3 = '      ||gradient||'
    #                 print(f'{str1:3}\t{str2:20}\t{str3:20}')
    #                 line_str = '-' * 80
    #                 print(line_str)
    #                 for i in range(len(native_result.geoopt_summary['energies'])):
    #                     energy = native_result.geoopt_summary['energies'][i]
    #                     gradient = native_result.geoopt_summary['gradient_norms'][i]
    #                     step = i
    #                     print(f'{step:3d}\t{energy:20.14f}\t{np.linalg.norm(gradient):20.14f}')
    #         else:
    #             warnings.warn('geoopt_summary() not availble in (LS)Dalton outputparser.
    #                           \nCheck convergence manually!')
    #             result.summary['energies'] = native_result.energy
    #             result.summary['geometries'] = native_result.final_geometry
    #             # better save than sorry
    #             result.summary['is_converged'] = False

    else:
        raise NotImplementedError

    result.summary['initial_geometry'] = initial_geometry
    return result.summary


class HygoOptimizer:
    """Simple optimizer class.

    Parameters
    ----------
    molecule : :obj:`Molecule`
        hygo molecule instance
    method : :obj:`Compute`
        hygo compute instance
    options : :obj:`dict`
        dictionary containing options for (in this case geometric)

    """

    def __init__(self, molecule: Molecule, method: Compute, options: Dict) -> None:
        self.molecule = molecule
        self.method = method
        self.options = options
        self.summary = self.optimization_loop()

    def optimization_loop(self):
        """Steepest descent optimization loop.

        Returns
        -------
        :obj:`dict`
            dictionary containing summary of the optimization

        """
        coordinates = np.array(self.molecule.coordinates.ravel())
        summary = {}
        summary['geometries'] = []
        summary['gradients'] = []
        summary['energies'] = []

        if self.options['hessian_update'] == 'BFGS':
            self.molecule.properties['hess_inv'] = np.identity(len(coordinates))

        if self.options['hessian_update'] == 'full':
            if not self.method.has_hessian:
                raise Exception(f'provided method {dir(self.method)} does not have a routine get_hessian()')
            self.molecule.properties['hess_inv'] = np.identity(len(coordinates))

        for step in range(self.options['steps_max']):
            # self.molecule.coordinates = coordinates.reshape(-1, 3)
            # if self.method.has_update:
            #     molecule_args = self.method.set_args(locals(), self.method.molecule_routine_args)
            #     molecule_kwargs = self.method.set_args(locals(), self.method.molecule_routine_kwargs)
            #     self.molecule = getattr(self.method.compute_class, self.method.molecule_routine)(*molecule_args,
            #                                                                                      **molecule_kwargs)
            # else:
            #     self.molecule.coordiante = coordinates.ravel().reshape(-1, 3)
            # molecule_args = self.method.set_args(locals(), self.method.molecule_routine_args)
            # molecule_kwargs = self.method.set_args(locals(), self.method.molecule_routine_kwargs)
            # try:
            #     getattr(self.method.compute_class, self.method.molecule_routine)(*molecule_args, **molecule_kwargs)
            # except Exception:
            #     raise Exception('could not update molecule on interface side')
            # finally:

            self.molecule.coordinates = coordinates.ravel().reshape(-1, 3)
            molecule = self.molecule

            energy_args = self.method.set_args(locals(), self.method.energy_routine_args)
            energy_kwargs = self.method.set_args(locals(), self.method.energy_routine_kwargs)

            energy = getattr(self.method.compute_class, self.method.energy_routine)(*energy_args, **energy_kwargs)

            self.molecule.properties['energy'] = energy

            gradient_args = self.method.set_args(locals(), self.method.gradient_routine_args)
            gradient_kwargs = self.method.set_args(locals(), self.method.gradient_routine_kwargs)

            gradient = getattr(self.method.compute_class, self.method.gradient_routine)(*gradient_args,
                                                                                        **gradient_kwargs)

            self.molecule.properties['gradient'] = gradient

            current_coord = coordinates.copy()
            summary['gradients'].append(gradient)
            summary['geometries'].append(current_coord.reshape(-1, 3))
            summary['energies'].append(energy)
            gradient = gradient.ravel()
            gradnorm = np.linalg.norm(gradient) / np.sqrt(self.molecule.num_atoms)
            steps_to_check = self.options['convergence_check_steps']
            summary['last_stable'] = -1
            if step >= steps_to_check:
                measures = check_iteration(
                    summary['energies'][-steps_to_check:],
                    summary['gradients'][-steps_to_check:],
                    summary['geometries'][-steps_to_check - 1:],
                    self.options,
                )
                if measures['is_stable']:
                    summary['last_stable'] = step

                if (step - summary['last_stable'] >= self.options['convergence_unstable_steps_allowed']):
                    summary['is_converged'] = False
                    warnings.warn('instability detected')
                    print('\n stability measures:')
                    try:
                        import json

                        found_json = True
                    except Exception:
                        found_json = None

                    if found_json is not None:
                        print(json.dumps(measures, sort_keys=False, indent=2))
                    else:
                        print(measures)
                    break

            if step > 1:
                endiff = np.abs(summary['energies'][-1] - summary['energies'][-2])
            else:
                endiff = 1e6
            iconv = 0
            iconv = (iconv + 1 if gradnorm < self.options['convergence_gradient_max'] else iconv)
            iconv = iconv + \
                1 if endiff < self.options['convergence_energy'] else iconv

            if iconv == 2:
                summary['is_converged'] = True
                break

            if all([self.options['constraints'] is not None, isinstance(self.options['constraints'], dict)]):
                if 'freeze' in self.options['constraints'].keys():
                    freezeopt = self.options['constraints']['freeze']
                    if freezeopt[0] == 'xyz':
                        for i in freezeopt[1:]:
                            # gradient = np.array(gradient).astype(np.float64)
                            gradient[i * 3:i*3 + 3] = np.float64(0.0)

                # could be done via the hylleraas Molecule methods

            if self.options['hessian_update'] is None:
                coordinates -= self.options['step_size'] * gradient
            else:
                if self.options['hessian_update'].lower() == 'bfgs':
                    self.molecule = self.update_hessian_bfgs(molecule)
                elif self.options['hessian_update'].lower() == 'full':
                    if gradnorm > 1e-3:
                        self.molecule = self.update_hessian_bfgs(molecule)
                    else:
                        self.molecule = self.update_hessian_full(molecule)

                coordinates = self.molecule.coordinates

            if self.options['print_level'] > 0:
                print(f'{step:3d}\t{energy:20.14f}\t{np.linalg.norm(gradient):20.14f}')

        if step >= self.options['steps_max'] - 1:
            warnings.warn(f'Optimization not converged within {step} steps')
            summary['is_converged'] = False

        return summary

    def update_hessian_full(self, mol: Any):
        """Update inverse of hessian using get_hessian() routine."""
        molecule = copy.deepcopy(mol)
        hessian_args = self.method.set_args(locals(), self.method.hessian_routine_args)
        hessian_kwargs = self.method.set_args(locals(), self.method.hessian_routine_kwargs)

        hessian = getattr(self.method.compute_class, self.method.hessian_routine)(*hessian_args, **hessian_kwargs)
        hessian = np.array(hessian)

        xk = np.array(molecule.coordinates).ravel()

        ek = mol.properties.get('energy', None)
        if ek is None:
            return

        gk = molecule.properties.get('gradient', None)
        if gk is None:
            return
        else:
            gk = np.array(gk).ravel()

        w = np.real(np.linalg.eigvals(hessian))

        shift = min(0, min(w)) * 1.1

        hess_inv = np.linalg.inv(hessian - shift * np.identity(len(hessian)))
        pk = -hess_inv @ gk
        sk = pk

        wolfe1 = False
        wolfe2 = False
        ak = 1.0
        mk = pk @ gk
        # primitive backtracking linesearch
        while (True):

            try:
                sk = ak * pk
                xl = xk + sk
                molecule = copy.deepcopy(mol)
                molecule.coordinates = xl.reshape(-1, 3)

                energy_args = self.method.set_args(locals(), self.method.energy_routine_args)
                energy_kwargs = self.method.set_args(locals(), self.method.energy_routine_kwargs)
                energy = getattr(self.method.compute_class, self.method.energy_routine)(*energy_args,
                                                                                        **energy_kwargs)

                gradient_args = self.method.set_args(locals(), self.method.gradient_routine_args)
                gradient_kwargs = self.method.set_args(locals(), self.method.gradient_routine_kwargs)
                gradient = getattr(self.method.compute_class, self.method.gradient_routine)(*gradient_args,
                                                                                            **gradient_kwargs)

            except Exception:
                ak *= 0.7
                continue
            el = energy
            gl = gradient.ravel()
            c1 = 0.9
            c2 = 0.5
            wolfe1 = (el <= ek + c1*ak*mk)
            wolfe2 = (-(pk @ gl) <= -c2 * mk)
            if all([wolfe1, wolfe2]):
                break
            ak *= 0.7

        xl = xk + sk

        molecule.coordinates = xl.reshape(-1, 3)
        molecule.properties['hess_inv'] = hess_inv

        return molecule

    def update_hessian_bfgs(self, mol: Any):
        """Update inverse of hessian using BFGS update."""
        ek = mol.properties.get('energy', None)
        if ek is None:
            return

        xk = np.array(mol.coordinates).ravel()
        gk = mol.properties.get('gradient', None)
        if gk is None:
            return
        else:
            gk = np.array(gk).ravel()

        bk = mol.properties.get('hess_inv', np.identity(len(gk)))

        pk = -(bk @ gk)
        mk = pk @ gk

        wolfe1 = False
        wolfe2 = False
        ak = 1.0
        # primitive backtracking linesearch
        for i in range(5):
            sk = ak * pk
            xl = xk + sk
            molecule = copy.deepcopy(mol)
            molecule.coordinates = xl.reshape(-1, 3)

            energy_args = self.method.set_args(locals(), self.method.energy_routine_args)
            energy_kwargs = self.method.set_args(locals(), self.method.energy_routine_kwargs)
            energy = getattr(self.method.compute_class, self.method.energy_routine)(*energy_args, **energy_kwargs)

            gradient_args = self.method.set_args(locals(), self.method.gradient_routine_args)
            gradient_kwargs = self.method.set_args(locals(), self.method.gradient_routine_kwargs)
            gradient = getattr(self.method.compute_class, self.method.gradient_routine)(*gradient_args,
                                                                                        **gradient_kwargs)

            el = energy
            gl = gradient.ravel()
            c1 = 0.9
            c2 = 0.5
            wolfe1 = (el <= ek + c1*ak*mk)
            wolfe2 = (-(pk @ gl) <= -c2 * mk)
            if all([wolfe1, wolfe2]):
                break
            ak *= 0.7

        yk = gl - gk
        sy = sk @ yk
        bfgs1 = (yk @ (bk@yk) / sy + 1.0) * np.outer(sk, sk) / sy
        bfgs2 = np.outer(bk @ yk, sk) / sy + np.outer(sk, yk) @ bk / sy
        hess_inv = bk + bfgs1 - bfgs2

        molecule.properties['energy'] = el
        molecule.properties['gradient'] = gl
        molecule.properties['hess_inv'] = hess_inv

        return molecule
