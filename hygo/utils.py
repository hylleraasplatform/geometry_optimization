# import collections.abc
# from typing import Dict, Optional
# import numpy as np

# def update(d: Dict, u: Dict) -> Dict:
#     """Update (nested) dictionary d with dictionary u."""
#     for k, v in u.items():
#         if isinstance(v, collections.abc.Mapping):
#             d[k] = update(d.get(k, {}), v)
#         else:
#             d[k] = v
#     return d

# def printf_dict(d: Dict, instring: Optional[str] = '', indent: Optional[int] = 2) -> str:
#     """Print (nested) dictionary LONDON style."""
#     string = ''
#     string += instring

#     for key, value in d.items():
#         if 'executable' in str(key):
#             continue
#         string += indent*' ' + str(key) + ' = '
#         if isinstance(value, Dict):
#             string += '{\n'
#             string += printf_dict(value, '', indent + 2)
#             if str(key) == 'system':
#                 string += 'LONDONATOMDEFINITION\n'
#             string += indent*' ' + '}\n'
#         else:
#             string += str(value) + '\n'
#     return string
