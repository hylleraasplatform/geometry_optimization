from typing import Dict, Optional


def set_options(options: Optional[Dict]) -> Dict:
    """Set and Checking optimizer options.

    Parameters
    ----------
    options : :obj:`dict`, optional
        input options

    Returns
    -------
    :obj:`dict`
        full list of options, checked and including default choices

    """
    if not options:
        options = {}
    options = set_default_options(options)

    check_options(options)

    return options


def set_default_options(options: Dict) -> Dict:
    """Set default optimizer options.

    Parameters
    ----------
    options : :obj:`dict`
        input options

    Returns
    -------
    :obj:`dict`
        full list of options including default choices

    """
    default: dict = {}
    default['optimizer'] = 'geometric'
    default['debug'] = False
    default['print_level'] = 1
    default['steps_max'] = 300
    default['stepsize'] = 0.001
    default['convergence_energy'] = 1e-6
    default['convergence_gradient_max'] = 3e-4
    default['convergence_gradient_rms'] = 5e-4
    default['convergence_step_max'] = 2e-3
    default['convergence_step_rms'] = 2e-3
    default['step_size'] = 0.5
    default['constraints'] = None
    default['convergence_check_steps'] = 50
    default['convergence_check_start'] = 50
    default['convergence_check_monotonicity_threshold'] = 1e-3
    default['convergence_check_oscillation_threshold'] = 1e-3
    default['convergence_check_goldstein_threshold'] = 5e-1
    default['convergence_check_orthogonality_threshold'] = 1e-4
    default['convergence_check_unstable_measures_allowed'] = 2
    default['convergence_unstable_steps_allowed'] = 3
    default['convergence_optking_criterions'] = None
    default['convergence_geometric_criterions'] = 'GAU'
    default['optimization_target'] = 'minimum'
    default['geometric_coordsys'] = 'tric'
    default['geometric_convergence_set'] = None

    default.update(options)

    return default


def check_options(options: Dict) -> None:
    """Check  optimizer options.

    Parameters
    ----------
    options : :obj:`dict`
        input options

    """
    for floats in [
            'convergence_energy',
            'convergence_gradient_max',
            'convergence_gradient_rms',
    ]:
        if not isinstance(options[floats], float):
            raise KeyError(f'option {floats} must be float')

    if not isinstance(options['optimizer'], str):
        raise KeyError('options "optimizer" must be str')

    if not isinstance(options['debug'], bool):
        raise KeyError('option "debug" must be boolean')

    if not isinstance(options['steps_max'], int):
        raise KeyError('option "" must be boolean')
