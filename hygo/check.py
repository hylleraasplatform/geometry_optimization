# BETA!

from typing import Dict

import numpy as np


def check_iteration(enlist, gradlist, geolist, options) -> Dict:
    """Check iteration.

    Parameters
    ----------
    enlist : :obj:`list`
        list of energies
    gradlist : :obj:`list`
        list of gradients
    geolist : :obj:`list` of :obj:`array`
        list of geometries
    options : :obj:`dict`, optional
        options for the optimizer

    Returns
    -------
    :obj:`dict`
        dictionary summarizing the iterations by means ov various measures

    """
    measures: Dict = {}
    # n_dim = len(enlist)
    n_atom = len(geolist[0])
    check_list = []
    step_list = []
    grad_list = []

    for i in range(len(enlist)):
        step_list.append((np.array(geolist[i + 1]) - np.array(geolist[i])).ravel())
        grad_list.append(np.array(gradlist[i]).ravel())

    measures['energy_difference'] = enlist[-1] - enlist[-2]
    measures['gradient_norm'] = np.linalg.norm(grad_list[-1])
    measures['step_norm'] = np.linalg.norm(step_list[-1])
    measures['gradient.step'] = np.dot(grad_list[-1], step_list[-1])
    if measures['gradient.step'] > options['convergence_check_orthogonality_threshold']:  # pragma: no cover
        check_list.append(1)
    else:
        check_list.append(0)

    angle = (measures['gradient.step'] / measures['gradient_norm'] / measures['step_norm'])
    if angle >= -1.0 and angle <= 1.0:
        measures['angle(gradient,step)'] = np.arccos(-angle) * 180.0 / np.pi
    else:
        measures['angle(gradient,step)'] = -1e4

    theta_thresh = (90.0 - np.arccos(options['convergence_check_orthogonality_threshold']) * 180.0 / np.pi)
    if np.abs(90.0 - measures['angle(gradient,step)']) <= theta_thresh \
            and measures['angle(gradient,step)'] > -1e3:  # pragma: no cover
        check_list.append(1)
    else:
        check_list.append(0)
    measures['Goldstein1'] = measures['energy_difference'] / \
        measures['gradient.step']
    if measures['Goldstein1'] < options['convergence_check_goldstein_threshold']:  # pragma: no cover
        check_list.append(1)
    else:
        check_list.append(0)

    monotonicity = check_monotonicity(enlist, options['convergence_check_monotonicity_threshold'])
    if monotonicity < 0:
        check_list.append(0)
        measures['energies'] = 'monotonically decreasing'
    else:  # pragma: no cover
        measures['energies'] = 'monotonically non-decreasing'
        check_list.append(1)

    s_oscillate = 0.0
    for i in range(len(step_list) - 2):
        v1 = np.array(step_list[i]) - np.array(step_list[i + 2])
        # v2 = np.array(step_list[i]) + np.array(step_list[i + 1])
        s_oscillate += np.linalg.norm(v1)**2
        # s_oscillate += np.linalg.norm(v2)**2

    s_oscillate = np.sqrt(s_oscillate) / n_atom
    if s_oscillate > options['convergence_check_oscillation_threshold']:  # pragma: no cover
        measures['step-oscillation_detected'] = True
        check_list.append(1)
    else:
        measures['step-oscillation_detected'] = False
        check_list.append(0)

    theta_stuck = 0.0
    for i in range(len(step_list)):
        overlap = np.dot(step_list[i], grad_list[i])
        step_norm = np.linalg.norm(step_list[i])
        grad_norm = np.linalg.norm(grad_list[i])
        angle = -overlap / step_norm / grad_norm
        if angle < -1.0:  # pragma: no cover
            angle = -1.0
        elif angle > 1.0:  # pragma: no cover
            angle = 1.0
        angle = np.arccos(angle) * 180.0 / np.pi
        theta_stuck += (angle - 90.0)**2

    theta_stuck = np.sqrt(theta_stuck / len(step_list))
    if theta_stuck < theta_thresh:  # pragma: no cover
        measures['step-trap_detected'] = True
        check_list.append(1)
    else:
        measures['step-trap_detected'] = False
        check_list.append(0)

    if sum(check_list) < options['convergence_check_unstable_measures_allowed']:
        measures['is_stable'] = True
    else:
        measures['is_stable'] = False  # pragma: no cover

    return measures


def check_monotonicity(energies, thresh) -> int:
    """Wrap monotonicity checker.

    Parameters
    ----------
    energies : :obj:`list`
        list of *n* energies
    thresh : :obj:`float`
        threshold (epsilon) for monotonicity fluctuations

    Returns
    -------
    :obj:`int`
        *(-m-1)*  the last *m<=n* iterations were monotonic decreasing
        1 the energies are monotonic increasing
        0 else

    """
    n = len(energies)
    is_monotonic_decreasing = False
    for n in range(0, n - 2):
        is_monotonic_decreasing = is_monotonic(energies[n:], thresh)
        if is_monotonic_decreasing:
            break

    if is_monotonic_decreasing:
        return -(n + 1)
    else:  # pragma: no cover
        is_monotonic_increasing = is_monotonic(-1 * energies[n - 2:], thresh)
        if is_monotonic_increasing:
            return 1
        else:
            return 0


def is_monotonic(a, epsilon):
    """Check monotonicity.

    Parameters
    ----------
    a : :obj:`list`
        list of values
    epsilon : :obj:`float`
        threshold for monotonicity fluctuations


    Returns
    -------
    :obj:`bool`
        wether the series is monotonic within +-epsilon

    """
    return all(a[i] + epsilon >= a[i + 1] for i in range(len(a) - 1))
