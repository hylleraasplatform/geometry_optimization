import resource
import sys
import time
from typing import Any, Dict, Optional

from .method import Compute
from .molecule import Molecule
from .optimizer import optimize
from .options import set_options


def geometry_optimization(molecule: Any,
                          method: Any,
                          options: Optional[Dict] = None,
                          compute_settings: Optional[Any] = None) -> Dict:
    """Hygo geometry optimization driver.

    Parameters
    ----------
    molecule : :obj:`str` or :obj:`hylleraas.Molecule` or :obj:`daltonproject.Molecule`
        molecule representation either as string, filename, daltonproject or hylleraas Molecule instance
    method : :obj:`dict` or *custom* :obj:`class`
        method for computation, either provided as a class or chosen from predefinitions via dictionary keys
    options : :obj:`dict`, optional
        options for the optimizer
    compute_settings : :obj:`Any`
        coming soon : computational settings

    Returns
    -------
    :obj:`dict`
        dictionary summarizing the geometry optimization

    """
    start_time = time.time()
    molecule = Molecule(molecule)
    method = Compute(method)
    options = set_options(options)

    # if options['debug']:
    #     print_all(molecule, method, options)

    result = optimize(molecule, method, options, compute_settings)

    mem_self = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    mem_children = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss
    mem = mem_self + mem_children
    if sys.platform != 'darwin':  # pragma no cover
        mem *= 1024
    end_time = time.time()
    result['walltime'] = end_time - start_time
    result['memory'] = mem

    if not result['is_converged']:
        print(f'\n Geometry optimization NOT converged in {len(result["energies"])-1} steps!')
        print(f' Last stable iteration : {result["last_stable"]}\n')
    else:
        print(f'\n Geometry optimization converged in {len(result["energies"])-1} steps.\n')

    return result


# def print_all(molecule, method, options: Dict) -> None:
#     """Print all options.

#     Parameters
#     ----------
#     molecule : :obj:`str` or :obj:`hylleraas.Molecule` or :obj:`daltonproject.Molecule`
#         molecule representation either as string, filename, daltonproject or hylleraas Molecule instance
#     method : :obj:`dict` or *custom* :obj:`class`
#         method for computation, either provided as a class or chosen from predefinitions via dictionary keys
#         (only the dictionary is printed though)
#     options : :obj:`dict`, optional
#         options for the optimizer

#     """
#     try: # pragma no cover
#         from pprint import pprint
#     except Exception:
#         import warnings
#         warnings.warn('pprint not available')
#         return

#     print('\n+++ DEBUG OUTPUT FOR ', molecule, ' +++\n')
#     pprint(vars(molecule))
#     if isinstance(method, Dict):
#         print('\n+++ DEBUG OUTPUT FOR ', method, ' +++\n')
#         pprint(vars(method))
#     print('\n+++ DEBUG OUTPUT FOR options dictionary ++\n')
#     pprint(options)
#     print('\n')

if __name__ == '__main__':
    import hylleraas as hsp
    mymol = hsp.Molecule('O 0 0 0\n H 0 0 -1 \n H 0 0.5 1')
    mymeth = hsp.Method({}, program='xtb')
    options = {'optimizer': 'BFGS'}

    test = geometry_optimization(mymol, mymeth, options)
