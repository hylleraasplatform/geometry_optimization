# # Note : this file is as of today (5.10.22) deprecated and will be removed in future release
# import os
# import shutil
# from abc import ABC, abstractmethod
# from typing import Dict, Optional

# import numpy as np

# from .utils import printf_dict
# from .utils import update as update_dict

# try:
#     import daltonproject as dp
#     found_dp = True
# except ImportError:  # pragma no cover
#     found_dp = False

# try:
#     from qcelemental import PhysicalConstantsContext, periodictable
#     constants = PhysicalConstantsContext('CODATA2018')
# except Exception:  # pragma no cover
#     raise Exception('qcelemental needed for running LONDON with hygo')

# class ProgramInterface(ABC):
#     """Factory class for interfaces."""

#     @staticmethod
#     @abstractmethod
#     def update_coordinates():
#         """Update self.molecule.coordinates."""
#         pass  # pragma no cover

#     @staticmethod
#     @abstractmethod
#     def get_gradient():
#         """Return gradient."""
#         pass  # pragma no cover

#     @staticmethod
#     @abstractmethod
#     def get_energy() -> float:
#         """Return energy."""
#         pass  # pragma no cover

# class InterfaceLSDalton(ProgramInterface):
#     """LSDalton interface.

#     Parameters
#     ----------
#     molecule : :obj:`daltonproject.Molecule`
#         daltonproject molecule instance
#     basis : :obj:`daltonproject.Basis`
#         daltonproject basis instance
#     qc_method : :obj:`daltonproject.QC_Method`
#         daltonproject QC_Method instance

#     """

#     def __init__(self, molecule, basis, qc_method):
#         if not found_dp:
#             raise Exception('daltonproject python package not available')
#         self.molecule = molecule
#         self.basis = basis
#         self.method = qc_method
#         self.properties = dp.Property(energy=True, gradients=True)

#     def update_coordinates(self, coordinates):
#         """Update coordinates."""
#         self.molecule.coordinates = coordinates.reshape(-1, 3)
#         return self.molecule

#     def get_energy(self):
#         """Compute energy (and gradient)."""
#         self.result = dp.lsdalton.compute(
#             molecule=self.molecule,
#             basis=self.basis,
#             qc_method=self.method,
#             properties=self.properties,
#         )
#         return self.result.energy

#     def get_gradient(self):
#         """Return gradient."""
#         return self.result.gradients

# class InterfaceDalton(ProgramInterface):
#     """Dalton interface.

#     Parameters
#     ----------
#     molecule : :obj:`daltonproject.Molecule`
#         daltonproject molecule instance
#     basis : :obj:`daltonproject.Basis`
#         daltonproject basis instance
#     qc_method : :obj:`daltonproject.QC_Method`
#         daltonproject QC_Method instance

#     """

#     def __init__(self, molecule, basis, qc_method):

#         # decoupling like this
#         # if inspect.isclass(type(Property)) :
#         #     Property(energy=True, gradients=True)
#         # else :
#         # dalotnproject not available
#         if not found_dp:
#             raise Exception('daltonproject python package not available')
#         self.molecule = molecule
#         self.basis = basis
#         self.method = qc_method
#         self.properties = dp.Property(energy=True, gradients=True)

#     def update_coordinates(self, coordinates):
#         """Update coordinates."""
#         self.molecule.coordinates = coordinates.reshape(-1, 3)
#         return self.molecule

#     def get_energy(self):
#         """Compute energy (and gradient)."""
#         self.result = dp.dalton.compute(
#             molecule=self.molecule,
#             basis=self.basis,
#             qc_method=self.method,
#             properties=self.properties,
#         )
#         return self.result.energy

#     def get_gradient(self):
#         """Return gradient."""
#         return self.result.gradients

# class InterfaceLondon(ProgramInterface):
#     """London interface.

#     Parameters
#     ----------
#     molecule_str : :obj:`str`
#         string containing xyz coordinates of molecule (in bohr), i.e. xyz file without the first two lines
#     options : :obj:`dict`, optional
#         options for London
#     name : :obj:`str`, optional
#         rootname of london in- and output files

#     """

#     def __init__(
#         self,
#         molecule_str: str,
#         options: Dict,
#         name: Optional[str] = None,
#     ):

#         self.executable = options.get('executable', 'london.x')
#         if shutil.which(self.executable) is None:
#             raise Exception(f'{self.executable} not found in PATH!!')

#         if name is None:
#             name = 'mylondoncalc'

#         self.filename_inp = name + str('.inp')
#         self.filename_out = name + str('.out')

#         self.atoms, self.coordinates = self.gen_molecule(molecule_str=molecule_str)
#         self.num_atoms = len(self.atoms)

#         self.scale = constants.bohr2angstroms
#         self.options = self.set_london_options(options)

#     def update_coordinates(self, coordinates):
#         """Update coordinates.

#         Parameters
#         ----------
#         coordinates : :obj:`array`
#             coordinates (in angstrom, from geomeTRIC)

#         Returns
#         -------
#         :obj:`array`
#             updates coordinates (in bohr) in :obj:`class Interface_London` instance.

#         """
#         self.coordinates = coordinates.ravel().reshape(-1, 3) / self.scale
#         return self.coordinates

#     def get_energy(self):
#         """Compute energy with LONDON.

#         Returns
#         -------
#         :obj:`float`
#             energy

#         """
#         write_file = open(self.filename_inp, 'w')
#         input_str = self.gen_london_input()
#         write_file.write(input_str)
#         write_file.close()
#         os.system(f'{self.executable} {self.filename_inp} > {self.filename_out}')
#         energy = self.london_output_parser_energy()
#         return energy

#     def get_gradient(self):
#         """Parse gradient from Energy computation with LONDON.

#         Returns
#         -------
#         :obj:`array`
#             gradient

#         """
#         gradient = self.london_output_parser_gradient().ravel()
#         return gradient

#     def london_output_parser_gradient(self) -> np.array:
#         """Extract gradient from LONDON output file.

#         Returns
#         -------
#         :obj:`np.array`
#             gradient

#         """
#         gradient = np.zeros((self.num_atoms, 3))
#         i = 0
#         with open(f'{self.filename_out}', 'r') as output_file:
#             for line in output_file:
#                 if 'Molecular Energy Gradient:' in line:
#                     found_grad = True
#                 if 'Ftotal' in line:
#                     split_line = (line.replace(',', ' ').replace(')', ' ').replace('(', ' ').split())
#                     gradient[i][0] = float(split_line[-3]) * self.scale
#                     gradient[i][1] = float(split_line[-2]) * self.scale
#                     gradient[i][2] = float(split_line[-1]) * self.scale
#                     i = i + 1
#         if not found_grad:
#             gradient[:] = 1e6
#         return gradient

#     def london_output_parser_energy(self) -> float:
#         """Extract energy from LONDON output file.

#         Returns
#         -------
#         :obj:`float`
#             energy

#         """
#         with open(f'{self.filename_out}', 'r') as output_file:
#             lines = output_file.readlines()
#         for line in lines:
#             if '(final) Total:' in line:
#                 val = float(line.split()[2])
#                 break

#         return val

#     def gen_london_input(self) -> str:
#         """Generate input for LONDON.

#         Returns
#         -------
#         :obj:`str`
#             input file

#         """
#         indent = 2
#         input = printf_dict(self.options, indent=indent)
#         london_atom_string = ''
#         for i in range(0, self.num_atoms):
#             atomic_charge = periodictable.to_atomic_number(self.atoms[i])
#             london_atom_string += (indent+2) * ' ' + 'charge = {:5.2f}\n'.format(float(atomic_charge))
#             london_atom_string += (indent+2) * ' ' + '{:>2}{:20.14f}{:20.14f}{:20.14f}\n'.format(
#                 self.atoms[i],
#                 self.coordinates[i][0] * self.scale,
#                 self.coordinates[i][1] * self.scale,
#                 self.coordinates[i][2] * self.scale,
#             )

#         return input.replace('LONDONATOMDEFINITION', london_atom_string.rstrip('\n'))

#         # input = ''
#         # for key0 in self.options:
#         #     if key0 == 'executable':
#         #         continue
#         #     input += key0 + '{\n'

#         #     for key1 in self.options[key0]:

#         #         if isinstance(key1, str):
#         #             input += '  ' + key1 + ' = ' + str(self.options[key0][key1]) + '\n'
#         #         else:
#         #             try:
#         #                 if isinstance(self.options[key0][key1], dict):
#         #                     input += '  ' + key1 + '{\n'
#         #                     for key2 in self.options[key0][key1]:
#         #                         input += ('    ' + key2 + ' = ' + str(self.options[key0][key1][key2]) + '\n')
#         #                     input += '  }\n'
#         #             except Exception:
#         #                 pass

#         #     if key0 == 'system':
#         #         for i in range(0, self.num_atoms):
#         #             atomic_charge = periodictable.to_atomic_number(self.atoms[i])
#         #             input += '  charge = {:5.2f}\n'.format(float(atomic_charge))
#         #             input += '  {:>2}{:20.14f}{:20.14f}{:20.14f}'.format(
#         #                 self.atoms[i],
#         #                 self.coordinates[i][0] * self.scale,
#         #                 self.coordinates[i][1] * self.scale,
#         #                 self.coordinates[i][2] * self.scale,
#         #             )
#         #             input += '\n'
#         #     input += '}\n'
#         # return input

#     @staticmethod
#     def gen_molecule(molecule_str: str):
#         """Generate molecule object for LONDON.

#         Parameters
#         ----------
#         molecule_str : :obj:`str`
#             string containing xyz coordinates (in bohr) of molecule (currently xyz file without the first two lines)

#         Returns
#         -------
#         :obj:`list`
#             atoms
#         :obj:`array`
#             coordinates (in angstrom)

#         """
#         atoms = []
#         lines = list(molecule_str.strip().split('\n'))
#         coordinates = np.zeros((len(lines), 3))
#         for i, line in enumerate(lines):
#             atom, *xyz = list(filter(None, line.strip().split(' ')))
#             atoms.append(atom)
#             coordinates[i] = np.array(list(map(float, xyz)), dtype=float) / constants.bohr2angstroms
#         return atoms, coordinates

#     @staticmethod
#     def set_london_options(options: Dict) -> Dict:
#         """Set options for LONDON.

#         Parameters
#         ----------
#         options : :obj:`dict`
#             user-defined options

#         Returns
#         -------
#         :obj:`dict`
#             full (default + user-defined) options dictionary for LONDON

#         """
#         # 1. read user defined options (check)
#         default_opt: dict = {}
#         default_opt['analysis'] = {
#             'analysis-only': 'no', 'xml-print-potential-basis-overlap': 'no',
#             'xml-print-potential-basis-kinetic': 'no', 'export-operators': 'no', 'linear-dependence-tol': 1e-06
#         }
#         default_opt['scf'] = {
#             'spin-symmetry-constraint': 'Restricted Hartree-Fock', 'linear-dependence-tol': 1e-06,
#             'uhf-spin-projection': 'free', 'disable-spin-zeeman-in-fock-matrix': 'no', 'noisy-init-guess': 'no',
#             'min-scf-iterations': 0, 'max-scf-iterations': 160, 'use-density-fitting': 'no', 'use-admm': 'no',
#             'analyze-using-dft-grid': 'no',
#             'diis': {'convergence-tolerance': 1e-06, 'subspace-dimension': 7, 'diagonalization-temperature': 0}
#         }
#         default_opt['casscf'] = {
#             'max-active-orbitals': 2000, 'number-of-frozen-orbitals': 0,
#             'number-of-states-in-casscf-optimization': 1, 'number-of-ci-states': 1, 'spin-projection': 'default',
#             'num-1exc-init-vec': 0, 'num-2exc-init-vec': 0, 'use-noisy-init-vec': 'no',
#             'ci-max-subspace-dim': 'default', 'ci-convergence-tol': 1e-06
#         }
#         default_opt['mp2'] = {
#             'spin-projection': 'default', 'number-of-weights': 8, 'basis-functions-per-block': 20,
#             'cs-tolerance': 0, 'print-mp2-components': 0
#         }

#         default_opt['rsp'] = {
#             'number-of-roots-requested': 1, 'root-selection-method': 'energy', 'excitation-basis': 'MObasis',
#             'starting-guess': 'UnitGuess', 'solver_choice': 'GenEig', 'method_choice': 'RPA',
#             'precondition': 'false', 'initial-subspace-dimension': 40, 'maximum-subspace-dimension': 240,
#             'convergence-tolerance': 1e-05, 'max-rsp-iterations': 100
#         }
#         default_opt['coupled-cluster'] = {
#             'truncation-level': 'CCSD', 'property-request': 'off', 'spin-projection': 'default', 'max-it': 150,
#             'write-t-amplitudes': 'yes', 'read-t-amplitudes': 'no',
#             'diis': {'convergence-tolerance': 1e-06, 'subspace-dimension': 7, 'diagonalization-temperature': 0}
#         }
#         default_opt['finite-difference'] = {
#             'run-finite-difference': 'false', 'run-type': 'gradient', 'use-ref-density': 'false',
#             'step-size': 0.0005
#         }

#         default_opt['system'] = {
#             'molecular-charge': 0, 'correlation-model': 'Hartree-Fock', 'hamiltonian': {
#                 'electron-mass': 1, 'speed-of-light': 137.036, 'adiabatic-connection-lambda': 1,
#                 'nuclear-charge-distribution': 'point charge', 'magnetic-field': (0, 0, 0), 'gauge-origin':
#                     (0, 0, 0), 'linear-magnetic-field': '(0, 0, 0),(0, 0, 0),(0, 0, 0)', 'linear-magnetic-origin':
#                         (0, 0, 0), 'electric-field': (0, 0, 0), 'electric-origin': (0, 0, 0),
#                 'linear-electric-field': '(0, 0, 0), (0,0,0), (0, 0, 0)',
#                 'include-basis-expanded-scalar-potential': 'no', 'include-basis-expanded-vector-potential': 'no',
#                 'include-repulsion-from-external-density': 'no', 'external-charge-density-scale-factor': 1,
#                 'include-coulomb-distance-to-external-density': 'no', 'external-charge-density-distance-factor': 1,
#                 'integral': {
#                     'use-coulomb-integral-permutation-symmetry': 'yes', 'use-cauchy-schwarz-screening': 'yes',
#                     'cauchy-schwarz-tol': 1e-15, 'boys-function-tol': 1e-17
#                 }, 'dft': {
#                     'jp-functional': 'none', 'vrg-functional': 'none', 'tau-correction': 'none',
#                     'vrg-hard-rs-cutoff': '62035.049', 'vrg-soft-density-cutoff': '1e-14',
#                     'exact-exchange-fraction': 'default', 'radial-grid-type': 'LMG', 'allow-grid-pruning': 'yes',
#                     'gc2-radial-grid-threshold': 1e-13, 'lebedev-angular-target-degree': 35,
#                     'lebedev-angular-minimal-degree': 15, 'weight-screen-threshold': 1e-20, 'becke-hardness': 3,
#                     'allow-becke-atom-size-correction': 'yes'
#                 }
#             }, 'use-london-orbitals': 'no', 'gto-contraction-type': 'all, contracted', 'basis': 'STO-3G'
#         }

#         user_opt = [item for item in options]
#         if 'system' not in user_opt:
#             raise KeyError('system key needed for generating LONDON input')

#         london_keywords = [
#             'analysis', 'scf', 'casscf', 'mp2', 'rsp', 'coupled-cluster', 'finite-difference', 'system'
#         ]
#         london_opt: dict = {}

#         for key in london_keywords:
#             if key in user_opt:
#                 london_opt[key] = update_dict(default_opt[key], options[key])

#         return london_opt
