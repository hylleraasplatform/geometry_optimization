import warnings
from typing import Dict

import numpy as np

from .check import check_iteration

try:
    import optking
except ImportError:
    raise ImportError('package optking not available')

# since psi4 is a conda package, we assume that it is not installed and thus dont throw an error immediately

try:
    import psi4
    found_psi4 = True
except Exception:
    warnings.warn('package psi4 not available', ImportWarning)
    found_psi4 = False

try:
    from qcelemental import PhysicalConstantsContext
    constants = PhysicalConstantsContext('CODATA2018')
    bohr2angstrom = constants.bohr2angstroms
except ImportError:  # pragma no cover (should be installed together with daltonproject)
    bohr2angstrom = 0.529177210903
    warnings.warn('qcelemental failed to import, continuing with old data', ImportWarning)


class OptkingOptimizer:
    """optking optimizer engine.

    Parameters
    ----------
    molecule : :obj:`Molecule`
        hygo molecule instance
    method : :obj:`Method'
        hygo method instance

    """

    def __init__(self, molecule, method, options: Dict):
        self.molecule = molecule
        self.method = method
        self.options = options
        self.summary: Dict = {}
        self.summary['geometries'] = []
        self.summary['gradients'] = []
        self.summary['energies'] = []

        molecule_str = ''
        for i, atom in enumerate(self.molecule.atoms):
            x = str(self.molecule.coordinates[i][0])
            y = str(self.molecule.coordinates[i][1])
            z = str(self.molecule.coordinates[i][2])
            molecule_str += f'{atom} {x} {y} {z}\n'

        if not found_psi4:
            raise ImportError('psi4 package not available')

        molecule = psi4.geometry(molecule_str)
        psi4.core.clean_options()

        constraints_dict = self.set_optking_kwargs(options)
        if self.options['convergence_optking_criterions'] is not None:
            dummy_str = self.options['convergence_optking_criterions']
        else:
            dummy_str = 'gau_verytight'
        psi4_options = {
            'basis': 'somebasis',
            'g_convergence': dummy_str,
            # "fixed_distance" :  (" 1  2  0.9 ")
            # "fixed_distance" : "2 3 2.75"
            # "frozen_distance" :  " 1  2 "
            # "frozen_distance" :  (" 1  2 ")
        }
        for key in constraints_dict:
            psi4_options[key] = constraints_dict[key]

        if self.options['optimization_target'].lower() == 'transition_state':
            psi4_options['opt_type'] = 'TS'

        psi4.set_options(psi4_options)
        opt = optking.OptHelper('name', comp_type='user')  # type: ignore
        opt.build_coordinates()

        if self.options['print_level'] > 0:
            str1 = ' it'
            str2 = '      energy'
            str3 = '      ||gradient||'
            print(f'{str1:3}\t{str2:20}\t{str3:20}')

        for step in range(self.options['steps_max']):
            coordinates = (opt.geom * bohr2angstrom).ravel()

            # molecule_args = self.method.set_args(locals(), self.method.molecule_routine_args)
            # molecule_kwargs = self.method.set_args(locals(), self.method.molecule_routine_kwargs)
            # try:
            #     getattr(self.method.compute_class, self.method.molecule_routine)(*molecule_args, **molecule_kwargs)
            # except Exception:
            #     raise Exception('could not update molecule on interface side')
            # finally:
            self.molecule.coordinates = coordinates.ravel().reshape(-1, 3)
            molecule = self.molecule

            energy_args = self.method.set_args(locals(), self.method.energy_routine_args)
            energy_kwargs = self.method.set_args(locals(), self.method.energy_routine_kwargs)
            energy = getattr(self.method.compute_class, self.method.energy_routine)(*energy_args, **energy_kwargs)

            gradient_args = self.method.set_args(locals(), self.method.gradient_routine_args)
            gradient_kwargs = self.method.set_args(locals(), self.method.gradient_routine_kwargs)

            gradient = getattr(self.method.compute_class, self.method.gradient_routine)(*gradient_args,
                                                                                        **gradient_kwargs)

            opt.E = energy
            opt.gX = gradient.ravel()
            opt.energy_gradient_hessian()
            opt.step()

            self.summary['energies'].append(energy)
            self.summary['gradients'].append(gradient)
            self.summary['geometries'].append(opt.geom * bohr2angstrom)

            self.steps_to_check = self.options['convergence_check_steps']
            self.summary['last_stable'] = -1
            if step >= self.steps_to_check:
                measures = check_iteration(
                    self.summary['energies'][-self.steps_to_check:],
                    self.summary['gradients'][-self.steps_to_check:],
                    self.summary['geometries'][-self.steps_to_check - 1:],
                    self.options,
                )
                if measures['is_stable']:
                    self.summary['last_stable'] = step

                if (step - self.summary['last_stable'] >= self.options['convergence_unstable_steps_allowed']):
                    self.summary['is_converged'] = False
                    warnings.warn('instability detected')
                    print('\n stability measures:')
                    try:
                        import json

                        found_json = True
                    except Exception:
                        found_json = None

                    if found_json is not None:
                        print(json.dumps(measures, sort_keys=False, indent=2))
                    else:
                        print(measures)
                    break

            if self.options['print_level'] > 0:
                print(f'{step:3d}\t{energy:20.14f}\t{np.linalg.norm(gradient):20.14f}')

            conv = (opt.test_convergence if self.options['convergence_optking_criterions'] is not None else False)
            if conv:
                self.summary['is_converged'] = True
                break

            gradnorm = np.linalg.norm(gradient.ravel()) / np.sqrt(self.molecule.num_atoms)
            if step > 1:
                endiff = np.abs(self.summary['energies'][-1] - self.summary['energies'][-2])
            else:
                endiff = 1e6

            iconv = 0
            iconv = (iconv + 1 if gradnorm < self.options['convergence_gradient_max'] else iconv)
            iconv = iconv + \
                1 if endiff < self.options['convergence_energy'] else iconv
            self.summary['is_converged'] = False
            if iconv == 2 and self.options['convergence_optking_criterions'] is None:
                self.summary['is_converged'] = True
                break

            if step == self.options['steps_max'] - 1:
                warnings.warn('maximum number of iterations reached.')

    @staticmethod
    def set_optking_kwargs(options: Dict) -> Dict:
        """Parser for optking options.

        Parameters
        ----------
        options : :obj:`dict`
            options for the optimizer

        Returns
        -------
        :obj:`dict`
            dictionary parsed to optking

        """
        if isinstance(options['constraints'], dict):
            constraints_dict = {}
            try:
                freezeopt = options['constraints']['freeze']
                string = 'frozen_' + freezeopt[0]
                constraints_dict[string] = ' '.join(map(str, freezeopt[1:]))
            except Exception:
                warnings.warn('optking currently only supports freezing')

        return constraints_dict
