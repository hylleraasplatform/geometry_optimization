import copy
import inspect
import warnings


class Compute:
    """Hygo compute class.

    Parameters
    ----------
    molecule : :obj:`Molecule`
        hygo molecule instance
    method : :obj:`dict` or :obj:'class'
        either dictionary for setting predefined compute class or a the class itself

    """

    def __init__(self, method):

        if isinstance(method, dict):

            self.compute_class = method.get('compute_class', None)
            self.energy_routine = method.get('energy_routine', None)
            self.energy_routine_args = method.get('energy_routine_args', [])
            self.energy_routine_kwargs = method.get('energy_routine_kwargs', {})
            self.gradient_routine = method.get('gradient_routine', None)
            self.gradient_routine_args = method.get('gradient_routine_args', [])
            self.gradient_routine_kwargs = method.get('gradient_routine_kwargs', {})

            # self.optimizer_routine = method.get("optimizer_routine", None)
            # self.optimizer_routine_args = method.get(
            #    "optimizer_routine_args", None)
            # self.optimizer_routine_kwargs = method.get(
            #    "optimizer_routine_kwargs", None)

        elif isinstance(method, list) or isinstance(method, str):
            raise Exception('geometry_optimization needs a method provided as a class or dictionary')

        elif inspect.isclass(type(method)):

            self.compute_class = method
            self.print_level = 0
            # if hasattr(method, 'print_level'):
            #     self.print_level = method.print_level

            for funcname in ['get_energy', 'get_gradient', 'get_hessian']:

                func = getattr(self.compute_class, funcname, None)

                if funcname == 'get_hessian':
                    if func is not None:
                        self.has_hessian = True
                    else:
                        self.has_hessian = False
                        continue

                if funcname == 'get_gradient':
                    if func is not None:
                        self.has_gradient = True
                    else:
                        self.has_gradient = False
                        # func = numerical gradient
                        # bind func to class

                if not callable(func):
                    raise AttributeError(f'compute_class {method.__class__} is missing method {funcname}!')

                fullarglist = inspect.signature(func)
                arglist: list = []
                kwarglist: dict = {}
                for param in fullarglist.parameters.values():

                    if param.kind == param.VAR_POSITIONAL or param.kind == param.VAR_KEYWORD:
                        warnings.warn(f'**args and **kwargs are ignored in function {funcname}!')
                        continue
                    if param.default is param.empty:
                        arglist.append(str(param.name))
                    else:
                        kwarglist[str(param.name)] = param.default

                arglist = None if arglist == [] else arglist

                # if self.print_level > 0:
                #     print(f'method {func} \n args : {arglist} \n kwargs {kwarglist}')

                if funcname == 'get_energy':
                    self.energy_routine = funcname
                    self.energy_routine_args = arglist
                    self.energy_routine_kwargs = kwarglist

                if funcname == 'get_gradient':
                    self.gradient_routine = funcname
                    self.gradient_routine_args = arglist
                    self.gradient_routine_kwargs = kwarglist

                if funcname == 'get_hessian':
                    self.hessian_routine = funcname
                    self.hessian_routine_args = arglist
                    self.hessian_routine_kwargs = kwarglist

    @staticmethod
    def set_args(local_dict, arglist_in):
        """Set arguments using local context.

        Parameters
        ----------
        local_dict : :obj:`dict`
            current variable context
        arglist_in : :obj:`str`  or :obj:`list` of :obj:`str`
            list of arguments (strings)

        Returns
        -------
        :obj:`list`
            updated arguments, stored in ``arglist``

        """
        arglist = copy.deepcopy(arglist_in)
        if arglist is None:
            return []
        if len(arglist) == 0:
            return arglist

        if isinstance(arglist, dict):
            for key in arglist:
                arg = str(arglist[key])
                try:
                    arglist[key] = local_dict[arg]
                except Exception:
                    arglist[key] = arg

        elif isinstance(arglist, list):
            for i, arg in enumerate(list(arglist)):
                if isinstance(arg, str):
                    try:
                        arglist[i] = local_dict[arg]
                    except Exception:
                        arglist[i] = arg
        elif isinstance(arglist, str):
            try:
                arglist = [local_dict[arglist]]
            except Exception:
                raise Exception(f'could not set args {arglist}')

        return arglist
