.. role:: python(code)
   :language: python

Constrained optimization
========================


   *This tutorial describes how to perform constrained geometry optimization.*


The complete Python script for these examples can be found `here <https://gitlab.com/hylleraasplatform/geometry_optimization/-/blob/main/examples/constrained_optimization.py>`_.


.. note:: In general, HyGO follows the input nomenclature from
   `geometric <https://geometric.readthedocs.io/en/latest/constraints.html>`_. While all options (`freeze`, `fix` (set) and `scan`) are available with :python:`optimizer=geometric`, *not* all of them work with :python:`optimizer=optking`.






Fixed bond distance
-------------------

To fix a bond distance between the second and fourth atom, add the corresponding `constraints` dictionary to the options:

.. literalinclude:: ../../../examples/constrained_optimization.py
   :lines: 15-20

Fixed bond angle
----------------

To freeze the angle spanned by the first three atoms with vertex located at the second atom, add ::

   "freeze": ["angle", 0, 1, 2]

to the `constraints` dictionary.

Fixed dihedral
--------------

For fixing the dihedral spanned by atoms 2--5, add::

   "freeze": ["dihedral", 2, 3, 4, 5]


Fixed cartesian coordinate
--------------------------

To fix a bond distance between the first and third atom, add the corresponding `constraints` dictionary to the options:

.. literalinclude:: ../../../examples/constrained_optimization.py
   :lines: 22-27

It is possible, to also fix only components of the cartesian coordinates, i.e., `x`, `y`, `z`, `xy`, `xz`, `yz`, `xyz`.
