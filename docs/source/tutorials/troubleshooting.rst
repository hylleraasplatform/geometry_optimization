.. role:: python(code)
   :language: python

Troubleshooting
===============

There is a myriad of reasons why a geometry optimization could fail.
Here, we only can give hints of how to identify these problems and maybe try to resolve them.


Check the return value(s)
-------------------------

Per default, the return dictionary contains information about the convergence::

   result = geomtry_optimization(molecule, method, options)
   if result["isconverged"]:
      print('geometry optimization converged!')

The optimizer also returns the what it assumes to be the last stable iteration::

   last_stable_iteration = result["last_stable"]
   last_stable_geometry = result["geometries"][last_stable_iteration]

which can be used for a restart.


Check the warnings
------------------

If the optimizer detects an instability during iterations, it will print the following measures:




.. table:: stability measures evaluated during optimization
   :widths: auto

   ============================== ================================================================================ ==========================================================
   measure                        expression                                                                       stability threshold
   ============================== ================================================================================ ==========================================================
   :python:`energy_difference`    :math:`\Delta E=E_{i}-E_{i-1}`                                                   all values accepted
   :python:`gradient_norm`        :math:`||g|| = ||g_{i}||_2`                                                      all values accepted
   :python:`step_norm`            :math:`||s|| = ||x_{i}-x_{i-1}||_2`                                              all values accepted
   :python:`gradient.step`        :math:`g.s=(g_{i}).(x_{i}-x_{i-1})`                                              :math:`g.s <10^{-4}` [[#f1]_]
   :python:`angle(gradient,step)` :math:`\theta_i=\cos^{-1}\frac{g.s}{||g||\cdot||s||}`                            :math:`|\theta_i-\frac{\pi}{2}|>\cos^{-1}(10^{-4})` [[#f1]_]
   :python:`Goldstein1`           :math:`\gamma=\frac{\Delta E}{g.s}`                                              :math:`\gamma\ge 0.5` [[#f2]_]
   monotonicity                   :math:`E_{i+1} \lessapprox E_{i}`                                                :math:`\forall i: E_{i+1}-E_{i}<10^{-3}` [[#f3]_]
   step-oscillation               :math:`\chi_s = \sqrt{\sum_i (x_{i}   - x_{i+2} + x_{i+1}-x_{i-1} )^2}`          :math:`\chi_s \ge 10^{-4}` [[#f4]_]
   step-trap                      :math:`\chi_\theta=\sqrt{\frac{1}{n-1}\sum_{i}(\theta_i-\frac{\pi}{2})^2}`       :math:`\chi_\theta \ge \cos^{-1}(10^{-4})` [[#f1]_]

   ============================== ================================================================================ ==========================================================


.. [#f1] threshold may be adjusted by overwriting  :python:`options["convergence_check_orthogonality_threshold"]`
.. [#f2] threshold may be adjusted by overwriting  :python:`options["convergence_check_goldstein_threshold"]`
.. [#f3] threshold may be adjusted by overwriting  :python:`options["convergence_check_monotonicity_threshold"]`
.. [#f4] threshold may be adjusted by overwriting  :python:`options["convergence_check_oscillation_threshold"]`

Where :math:`E_i` is the energy in iteration :math:`i\le n`, :math:`x_i` is the corresponding geometry and :math:`g_i` the gradient.
The optimizer allows maximal 2 (controlled by :python:`options["convergence_check_unstable_measures_allowed"]`) criterions to be unstable in order to assess the current iteration as stable.


The last three criteria can give hints why the geometry optimization fails.
In particular, the detection of oscillations or trapped iterations can help to fix this problem.



Change the optimizer engine
---------------------------

`optking`_ and `geometric`_ use different parametrizations for internal coordinates. This can be used to create fallback solutions in geometry optimizations, e.g.::

   import hygo
   import warnings
   max_numer_of_iterations = 300
   conv = False
   total_number_of_iterations = 0
   while not conv:
      geoopt = hygo.geometry_optimization(molecule, method, options)
      conv = geopt['is_converged']
      if not conv:
         molecule.coordinates = geoopt['geometries'][geoopt['last_stable'] - 1]
      total_number_of_iterations += len(geopt['energies'])
      if total_number_of_iteration >= max_numer_of_iterations:
         warnings.warn('geometry optimization exceeded number of iterations')
         break
      if not conv:
         if options['optimizer'] == 'optking':
            options['optimizer'] = 'geometric'
            # restart from last stable geometry
               molecule.coordinates = geoopt['geometries'][geoopt['last_stable']]
         elif options['optimizer'] == 'geometric':
            options['optimizer'] = 'optking'
            # restart from initial geometry
            molecule.coordinates = geoopt['geometries'][0]
   final_geometry = geopt['geometries'][-1]
   final_gradient = geopt['gradients'][-1]
   final_energy = geopt['energies'][-1]



.. _optking:
   https://github.com/psi4/psi4/tree/master/psi4/src/psi4/optking


.. _geometric:
   https://github.com/leeping/geomeTRIC



Change the internal coordinate system
-------------------------------------

geomtric offers the option of choosing the internal coordinate system. The following systems are available:

.. table:: internal coordinates to-be used with geometric
   :widths: auto

   ====== =======================================================================
   option description
   ====== =======================================================================
   tric   Translation-Rotation Internal Coordinates (default)
   cart   Cartesian coordinate system
   prim   Primitive (a.k.a redundant internal coordinates)
   dlc    Delocalized Internal Coordinates
   hdlc   Hybrid Delocalized Internal Coordinates
   tric-p Primitive Translation-Rotation Internal Coordinates (no delocalization)
   ====== =======================================================================


In hygo, these can be choosen by setting :python:`options["geometric_coordsys"]` to the respective option.
