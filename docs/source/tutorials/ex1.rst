.. role:: python(code)
   :language: python

Fully customized optimization
-----------------------------


   *This tutorial describes how to provide own routines to the geometry optimizer.*



Assume we want to perform a DFT transition-state geometry optimization for the water molecule  with different functionals using energies and gradients provided by lsdalton via daltonproject.

First, we need to import hygo and daltonproject and initiate the geometry:

.. literalinclude:: ../../../examples/custom_functions.py
   :lines: 2-9

Then, we create the class that is later handed over to the optimizer. It should contain three methods for

#. :python:`get_energy()`: returning the energy as a *float*
#. :python:`get_gradient()`: returning the gradient as an array of dimension *(number of atoms, 3)*
#. :python:`update_coordinates()`: extracting the coordinates as an array of dimension *(number of atoms, 3)*

.. literalinclude:: ../../../examples/custom_functions.py
   :lines: 11-38

Then, the class is instantiated (in this case with the requested functional)

.. literalinclude:: ../../../examples/custom_functions.py
   :lines: 41

The optimizer will (try to) automatically extract all arguments for these methods and replace them with the current values at runtime, see :py:meth:`src.method.Compute.set_args`.

Then, the optimizer options are set:

.. literalinclude:: ../../../examples/custom_functions.py
   :lines: 43-48


And finally, the optimization is performed, passing the molecule, method and options to the optimizer:

.. literalinclude:: ../../../examples/custom_functions.py
   :lines: 50

The complete Python script for this example can be found `here <https://gitlab.com/hylleraasplatform/geometry_optimization/-/blob/main/examples/custom_functions.py>`_.
