.. role:: python(code)
   :language: python


Optimization with LONDON
=========================


The complete Python script for these examples can be found `here <https://gitlab.com/hylleraasplatform/geometry_optimization/-/blob/main/examples/london.py>`_.


The following stes are necessary (on saga) to run this optimization

#. install London (from now, we assume ``london.x`` is in ``PATH`` as well as ``baslib`` is available in the directory the computation is performed in)
#. ``pip3 install --user --upgrade numpy qcelemental geometric``
#. ``pip3 install --user --upgrade --force-reinstall git+https://gitlab.com/hylleraasplatform/geometry_optimization.git``
#. ``module load OpenBLAS/0.3.18-GCC-11.2.0``


The code for the optimziation follows:

.. literalinclude:: ../../../examples/london.py
   :lines: 2-

As output you will see this:

.. literalinclude:: ../../../examples/london.out
   :lines: 1-

Concerning the convergence criteria, note the following from the geometric code::

   # convergence dictionary to store criteria stored in order of energy, grms, gmax, drms, dmax
   # 'GAU' contains the default convergence criteria that are used when nothing is passed.
   convergence_sets = {'GAU': [1e-6, 3e-4, 4.5e-4, 1.2e-3, 1.8e-3],
                        'NWCHEM_LOOSE': [1e-6, 3e-3, 4.5e-3, 3.6e-3, 5.4e-3],
                        'GAU_LOOSE': [1e-6, 1.7e-3, 2.5e-3, 6.7e-3, 1e-2],
                        'TURBOMOLE': [1e-6, 5e-4, 1e-3, 5.0e-4, 1e-3],
                        'INTERFRAG_TIGHT': [1e-6, 1e-5, 1.5e-5, 4.0e-4, 6.0e-4],
                        'GAU_TIGHT': [1e-6, 1e-5, 1.5e-5, 4e-5, 6e-5],
                        'GAU_VERYTIGHT': [1e-6, 1e-6, 2e-6, 4e-6, 6e-6]}

The default value is ``GAU``.



Custom Optimization with LONDON
===============================


The complete Python script for these examples can be found `there <https://gitlab.com/hylleraasplatform/geometry_optimization/-/blob/main/examples/custom_london.py>`_.


The following stes are necessary (on saga) to run this optimization

#. install London (from now, we assume ``london.x`` is in ``PATH``)
#. ``pip3 install --user --upgrade numpy qcelemental geometric``
#. ``pip3 install --user --upgrade git+https://gitlab.com/hylleraasplatform/geometry_optimization.git``
#. ``module load OpenBLAS/0.3.18-GCC-11.2.0``


The code for the optimziation follows:

.. literalinclude:: ../../../examples/custom_london.py
   :lines: 2-
