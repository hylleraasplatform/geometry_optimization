# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath('../..'))
# -- Project information -----------------------------------------------------

project = 'HyGO'
copyright = '2022, Hylleraas Centre'
author = 'tilmann bodenstein'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.mathjax', 'sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx.ext.autosectionlabel',
    'sphinx.ext.viewcode', 'autoapi.extension', 'sphinx.ext.todo', 'sphinx.ext.intersphinx', 'myst_parser'
]
# what and where the sphinx-autoapi is looking for
autoapi_type = 'python'
autoapi_dirs = ['../../hygo']

# The root document.
root_doc = 'index'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
# exclude_patterns = []

source_suffix = ['.rst', '.md', '.txt']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_material'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_logo = 'hygologo_white.pdf'
html_show_sphinx = False

python_use_unqualified_type_names = True

html_theme_options = {
    'nav_title': 'HyGO Documentation',
    # Specify a base_url used to generate sitemap.xml. If not
    # specified, then no sitemap will be built.
    # 'base_url': 'https://project.github.io/project',
    # Colors
    # The theme color for mobile browsers. Hex color.
    'theme_color': '#3f51b5',
    # Primary colors:
    # red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
    # teal, green, light-green, lime, yellow, amber, orange, deep-orange,
    # brown, grey, blue-grey, white
    'color_primary': 'blue-grey',
    # Accent colors:
    # red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
    # teal, green, light-green, lime, yellow, amber, orange, deep-orange
    'color_accent': 'amber',
    'repo_url': 'https://gitlab.com/hylleraasplatform/geometry_optimization',
    'repo_name': 'hygo',
    'repo_type': 'gitlab',
    'globaltoc_depth': 2,
    'globaltoc_collapse': True,
    'globaltoc_includehidden': False,
    'html_minify': False,
    'html_prettify': True,
    # links added to the top bar permanently additionally to bread crums
    'nav_links': [{
        'href': 'https://hylleraas.readthedocs.io/en/latest/', 'internal': False,
        'title': 'Hylleraas Software Platform'
    }, ],
    'master_doc': True,
    'heroes': {'index': 'HyGO: standalone hylleraas package for geometry optimizations'},
}

html_short_title = 'HyGO : Hylleraas Geometry Optimizer'
html_title = 'HyGO : Hylleraas Geometry Optimizer'
html_show_sourcelink = True
html_sidebars = {'**': ['searchbox.html', 'logo-text.html', 'localtoc.html', 'globaltoc.html', 'relations.html']}
