Plans, Perspectives, Ideas
==========================
.. toctree::
   :maxdepth: 2


Monitoring Resources
--------------------

Until now, HyGO just measures and returns peak maximum memory and total wall time. This will be extended to monitor the individual iterations.


Monitoring Steps
----------------

We plan to include a monitoring of the update steps. E.g., if the update is confined on functional groups, or is concerned translational or rotational degrees of freedom. This may be moved to an external module which, e.g. can also perform shape analysis.

Metadynamics
------------

A quick computation which gives an overview of the energy landscape could be useful for many users. This may be based on metadynamics.


Advanced Fallback Optimizer
---------------------------

Until now, there is only a steepest descent optimizer in addition to Optking and Geometric. This should be extended first by including a simple line search and then more advanced techniques.


Advanced Transition State Search
--------------------------------

This could be added to the fallback optimizer and include successfull techniques for transition state search as, e.g. described in this article:

   Peng, C., & Bernhard Schlegel, H. (1993). Combining Synchronous Transit and Quasi-Newton Methods to Find Transition States. Israel Journal of Chemistry, 33(4), 449–454. doi:10.1002/ijch.199300051
