.. image:: hygologo_black.pdf
   :width: 240



=============
Documentation
=============

.. toctree::
   :maxdepth: 2

   installation.rst
   tutorials.rst
   plans.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
