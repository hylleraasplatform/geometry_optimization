Getting Started
===============


.. toctree::


Installation
------------

HyGO ist part of the `Hylleraas Software Platform <https://hylleraas-sphinx-test.readthedocs.io/en/new_structure/>`_ and installed via

.. code-block:: bash

    pip install hylleraas


However, the program can be used stand-alone and installed via git

.. code-block:: bash

    pip install git+https://gitlab.com/hylleraasplatform/geometry_optimization.git

For that to work, one needs to have `numpy` and `qcelemental` installed (e.g. via pip)

Quickstart
----------

To set the initial molecular geometry, one can setup a literal string:

.. literalinclude:: ../../examples/quickstart.py
   :lines: 4-9

or refer to a `.xyz`, `.zmat` file instead::

    molecule_string = 'mymol.xyz'

Let's assume we want to perform a DFT/B3LYP computation with the cc-pVDZ basis set using LSDalton via `daltonproject <https://daltonproject.readthedocs.io>`_. We can use the standard daltonproject input to set-up the method:

.. literalinclude:: ../../examples/quickstart.py
   :lines: 2-3,10-11

With these, we can set-up HyGO's method dictionary:

.. literalinclude:: ../../examples/quickstart.py
   :lines: 13

Next, we set up the options for HyGO. In this case, we want to use geometric as optimizer engine (also accepted are `optking`, `native` or `steepestdescent`):

.. literalinclude:: ../../examples/quickstart.py
   :lines: 14

Finally, we can perform the computation:

.. literalinclude:: ../../examples/quickstart.py
   :lines: 15

and extract the final coordinates and energy:

.. literalinclude:: ../../examples/quickstart.py
   :lines: 16-17


The complete Python script for this example can be found `here <https://gitlab.com/hylleraasplatform/geometry_optimization/-/blob/main/examples/quickstart.py>`_.
