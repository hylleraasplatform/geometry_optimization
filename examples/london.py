import numpy as np

import hygo

# input geometry
h4 = """H  -1.700000 -1.062500 0.000000
H   1.30000  -0.562500 0.000000
H   -1.000000 -0.062500 0.000000
H   1.400000 1.687500 0.000000
"""

# function to write molecule string like above from atoms and coordinates


def write_coordinates(atoms, coordinates, scale):
    """Update coordinates."""
    mol_str = ''
    for i, atom in enumerate(atoms):
        mol_str += '{:>2}{:20.14f}{:20.14f}{:20.14f}\n'.format(
            atom,
            coordinates[i][0] * scale,
            coordinates[i][1] * scale,
            coordinates[i][2] * scale,
        )
    return mol_str


# options for london
myopt = {
    'scf': {'uhf-spin-projection': -4, 'diis': {'convergence-tolerance': 1e-9}},
    'system': {
        'basis': 'cc-pVDZ',
        'gto-contraction-type': 'contracted',
        'hamiltonian': {'magnetic-field': (0, 0, 1.0e0)},
        'molecular-charge': ' 0',
    },
}

# options for hygo
pre_opt = {'program': 'london', 'options': myopt}

# options for optimizer
geo_options = {
    'optimizer': 'geometric',
    'geometric_coordsys': 'tric',
    'geometric_convergence_set': 'GAU_TIGHT',
}

# computation
geoopt_pre = hygo.geometry_optimization(molecule=h4, method=pre_opt, options=geo_options)

# update coordinates
h4_updated = write_coordinates(geoopt_pre['atoms'], geoopt_pre['final_geometry_bohr'], 1.0)

# options for london
myopt_big = {
    'scf': {'uhf-spin-projection': -4, 'diis': {'convergence-tolerance': 1e-9}},
    'system': {
        'basis': 'aug-cc-pVTZ',
        'gto-contraction-type': 'normalized primitive',
        'hamiltonian': {'magnetic-field': (0, 0, 1.0e0)},
        'molecular-charge': ' 0',
    },
}

# options for hygo
post_opt = {'program': 'london', 'options': myopt_big}

# updated options for optimizer
geo_options['geometric_convergence_set'] = 'GAU_VERYTIGHT'
geoopt_post = hygo.geometry_optimization(molecule=h4_updated, method=post_opt, options=geo_options)
# update coordinates
h4_converged = write_coordinates(geoopt_post['atoms'], geoopt_post['final_geometry_bohr'], 1.0)
print(h4_converged)

# final results
print('FINAL ENERGY = ', geoopt_post['final_energy'])
print(
    'FINAL GEOMETRY (angstrom)= ',
    np.round_(geoopt_post['final_geometry_angstrom'], decimals=8, out=None),
)
print(
    'FINAL GEOMETRY (bohr)= ',
    np.round_(geoopt_post['final_geometry_bohr'], decimals=8, out=None),
)
