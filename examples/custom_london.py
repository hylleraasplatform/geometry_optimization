# NB: if you change this file, remember to update the documentation
import os
from typing import Dict, Optional

import numpy as np
from qcelemental import PhysicalConstantsContext

import hygo

constants = PhysicalConstantsContext('CODATA2018')

h2 = """ H            -1.3            0          0
    H            1.3            0          0
"""

h4 = """ H            -1.3            0          0
    H            1.3            0          0
    H            -1.3            2.6          0.0
    H            1.3            2.6          0.0
"""


class LondonGeo:
    """Class for performing geometry optimizations with LONDON."""

    def __init__(self, molecule_str: str, options: Optional[Dict] = None):
        self.name = 'testcalc'
        self.filename_inp = self.name + str('.inp')
        self.filename_out = self.name + str('.out')
        if options is not None:
            self.options = self.set_default_london_options(options)
        self.atoms, self.coordinates = self.gen_molecule(molecule_str=molecule_str)
        self.num_atoms = len(self.atoms)
        self.scale = 1.0 / constants.bohr2angstroms

    def update_coordinates(self, coordinates):
        """Update and scale coordinates."""
        self.coordinates = coordinates.ravel().reshape(-1, 3) * self.scale
        return self.coordinates

    def get_energy(self):
        """Compute energy with LONDON."""
        write_file = open(self.filename_inp, 'w')
        input_str = self.gen_london_input()
        write_file.write(input_str)
        write_file.close()
        os.system(f'./london.x {self.filename_inp} > {self.filename_out}')
        energy = self.london_output_parser_energy()
        return energy

    def get_gradient(self):
        """Parse and return gradient."""
        gradient = self.london_output_parser_gradient().ravel()
        return gradient

    def london_output_parser_gradient(self) -> np.array:
        """Extract gradient from LONDON output file."""
        gradient = np.zeros((self.num_atoms, 3))
        i = 0
        with open(f'{self.filename_out}', 'r') as output_file:
            for line in output_file:
                if 'Molecular Energy Gradient:' in line:
                    found_grad = True
                if 'Ftotal' in line:
                    split_line = (line.replace(',', ' ').replace(')', ' ').replace('(', ' ').split())
                    gradient[i][0] = float(split_line[-3]) / self.scale
                    gradient[i][1] = float(split_line[-2]) / self.scale
                    gradient[i][2] = float(split_line[-1]) / self.scale
                    i = i + 1
        if not found_grad:
            raise Exception('gradient not found in outputfile')
        return gradient

    def london_output_parser_energy(self) -> float:
        """Extract energy from LONDON output file."""
        with open(f'{self.filename_out}', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if '(final) Total:' in line:
                val = float(line.split()[2])
                break
        return val

    def gen_london_input(self) -> str:
        """Generate LONDON input."""
        input_str = ''
        for key0 in self.options:
            input_str += key0 + '{\n'
            for key1 in self.options[key0]:

                if isinstance(self.options[key0][key1], dict):
                    input_str += '\t' + key1 + '{\n'
                    for key2 in self.options[key0][key1]:
                        input_str += ('\t\t' + key2 + ' = ' + str(self.options[key0][key1][key2]) + '\n')
                    input_str += '\t}\n'
                else:
                    input_str += '\t' + key1 + ' = ' + \
                        str(self.options[key0][key1]) + '\n'
            if key0 == 'system':
                for i in range(0, self.num_atoms):
                    input_str += '\t{:>2}{:8.3f}{:8.3f}{:8.3f}'.format(
                        self.atoms[i],
                        self.coordinates[i][0],
                        self.coordinates[i][1],
                        self.coordinates[i][2],
                    )
                    input_str += '\n'
            input_str += '}\n'
        return input_str

    @staticmethod
    def gen_molecule(molecule_str: str):
        """Generate LONDON molecule from string."""
        atoms = []
        lines = list(molecule_str.strip().split('\n'))
        coordinates = np.zeros((len(lines), 3))
        for i, line in enumerate(lines):
            atom, *xyz = list(filter(None, line.strip().split(' ')))
            atoms.append(atom)
            coordinates[i] = np.array(list(map(float, xyz)), dtype=float)
        return atoms, coordinates

    @staticmethod
    def set_default_london_options(options: Dict) -> Dict:
        """Set default LONDON options."""
        scfopt = options.get('scf', {})
        diisopt = scfopt.get('diis', {})
        systemopt = options.get('system', {})
        hamilopt = systemopt.get('hamiltonian', {})

        diisopt.setdefault('convergence-tolerance', 1e-06)

        scfopt.setdefault('spin-symmetry-constraint', 'Unrestricted Hartree-Fock')
        scfopt.setdefault('uhf-spin-projection', 0)
        scfopt.setdefault('property-request', 'geometrical-gradient')
        scfopt.setdefault('disable-spin-zeeman-in-fock-matrix', 'yes')
        scfopt.setdefault('diis', diisopt)

        hamilopt.setdefault('magnetic-field', (0, 0, 0))
        hamilopt.setdefault('gauge-origin', (0, 0, 0))

        systemopt.setdefault('hamiltonian', hamilopt)
        systemopt.setdefault('molecular-charge', 0)
        systemopt.setdefault('correlation-model', 'Hartree-Fock')
        systemopt.setdefault('use-london-orbitals', 'yes')
        systemopt.setdefault('gto-contraction-type', 'normalized primitive')
        systemopt.setdefault('basis', 'aug-cc-pVTZ')
        systemopt.setdefault('charge', 1)

        options.setdefault('scf', scfopt)
        options.setdefault('system', systemopt)

        return options


myopt = {
    'scf': {'uhf-spin-projection': 0},
    'system': {'basis': 'aug-cc-pVDZ', 'hamiltonian': {'magnetic-field': (0, 0, 0)}},
}
mycalc = LondonGeo(h2, options=myopt)

geo_options = {
    'optimizer': 'geometric',
    'convergence_energy': 1e-6,
    'geometric_coordsys': 'tric',
}

geoopt = hygo.geometry_optimization(molecule=h2, method=mycalc, options=geo_options)
print('FINAL ENERGY = ', geoopt['energies'][-1])
print('FINAL GEOMETRY = ', geoopt['geometries'][-1])
