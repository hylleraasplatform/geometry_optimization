# NB: if you change this file, remember to update the documentation ex2.rst
import daltonproject as dp

import hygo

molecule_string = """
   O 0 0 0
   H 0 0 1.795239827225189
   H 1.693194615993441 0 -0.599043184453037"""

molecule_string = """
   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.7394416921"""

b3lyp = dp.QCMethod('DFT', 'BP86')
my_basis = dp.Basis(basis='cc-pVDZ')
my_molecule = dp.Molecule(atoms=molecule_string)
my_method = {'program': 'lsdalton', 'basis': my_basis, 'method': b3lyp}

geo_options_geometric = {
    'optimizer': 'geometric',
    'print_level': 1,
    'optimization_target': 'minimum',
    'constraints': {
        # freeze distance between first and third atom
        'freeze': ['distance', 2, 3],
        'steps_max': 100,
    },
}
geo_options_optking = {
    'optimizer': 'optking',
    'print_level': 1,
    'optimization_target': 'minimum',
    'constraints': {
        # freeze xyz coordinate of the first atom, i.e. molecule.coordinates[0]
        'freeze': ['cartesian', 1, 'xyz']
    },
}

geo1 = hygo.geometry_optimization(molecule=my_molecule, method=my_method, options=geo_options_geometric)

geo2 = hygo.geometry_optimization(molecule=my_molecule, method=my_method, options=geo_options_optking)
