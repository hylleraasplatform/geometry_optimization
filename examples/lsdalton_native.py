import daltonproject as dp

import hygo

molecule_string = """
   O 0 0 0
   H 0 0 1.795239827225189
   H 1.693194615993441 0 -0.599043184453037"""

b3lyp = dp.QCMethod('DFT', 'B3LYP')
my_basis = dp.Basis(basis='cc-pVDZ')
my_molecule = dp.Molecule(atoms=molecule_string)

print(dir(my_molecule))
print(my_molecule.elements)
print(type(my_molecule.elements))
print(isinstance(my_molecule.elements, list))
print(my_molecule.atoms)
print(type(my_molecule.atoms))

geo_options = {'optimizer': 'native', 'optimization_target': 'transition_state'}
lsdalton_dft = {'program': 'lsdalton', 'basis': my_basis, 'method': b3lyp}
geoopt = hygo.geometry_optimization(molecule=my_molecule, method=lsdalton_dft, options=geo_options)
