# NB: if you change this file, remember to update the documentation ext1.rst
import daltonproject as dp

import hygo

molecule_string = """
   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""


class CalcDft:
    """Hygo compute class for optimizing geometry using user-defined DFT-functionals."""

    def __init__(self, functional):
        self.water = dp.Molecule(atoms=molecule_string)
        self.dft = dp.QCMethod('DFT', functional)
        self.basis = dp.Basis(basis='cc-pVDZ')
        self.property_energy = dp.Property(energy=True)
        self.property_gradient = dp.Property(energy=True, gradients=True)

    def update_coordinates(self, coordinates):
        """Update molecular coordinates."""
        self.water.coordinates = coordinates.ravel().reshape(-1, 3)
        return self.water

    def get_energy(self, ref=1e6):
        """Compute energy."""
        result = dp.lsdalton.compute(self.water, self.basis, self.dft, self.property_energy)
        if result.energy is None:
            return ref
        else:
            return result.energy

    def get_gradient(self):
        """Compute gradient."""
        result = dp.lsdalton.compute(self.water, self.basis, self.dft, self.property_gradient)
        return result.gradients


mycalc = CalcDft('B3LYP')

geo_options = {
    'optimizer': 'geometric',
    'convergence_energy': 1e-7,
    'optimization_target': 'transition_state',
    'print_level': 1,
}

geoopt = hygo.geometry_optimization(molecule=molecule_string, method=mycalc, options=geo_options)
