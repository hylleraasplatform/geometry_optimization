# NB! when you change this file, remember to update the documentation in installation.rst
import daltonproject as dp

import hygo

molecule_string = """
   O 0 0 0
   H 0 0 1.795239827225189
   H 1.693194615993441 0 -0.599043184453037"""
b3lyp = dp.QCMethod('DFT', 'B3LYP')
my_basis = dp.Basis(basis='cc-pVDZ')
my_molecule = dp.Molecule(atoms=molecule_string)
my_method = {'program': 'lsdalton', 'basis': my_basis, 'method': b3lyp}
geo_options = {'optimizer': 'geometric'}
geo_opt = hygo.geometry_optimization(molecule=my_molecule, method=my_method, options=geo_options)
final_energy = geo_opt['energies'][-1]
final_geometry = geo_opt['geometries'][-1]
