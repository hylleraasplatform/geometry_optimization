# HyGO
Hylleraas Geometry Optimizer

## Package description

This hylleraas-package is a standalone module for performing
geometry optimizations with molecular softwares using several
optimizers. Available are:

*   [geomeTRIC](<https://geometric.readthedocs.io/en/latest/>)
*   [OPTKING](<https://github.com/psi-rking/optking/tree/master/optking>)
*   steepest-descent

It is generally possible, but not thoroughly tested, to perform
constrained optimization with freezing both cartesian and internal
coordinates, as well as transition state search.

## Documentation
The documentation can be found [here](<https://geometryoptimizer.readthedocs.io>)

## Installation

HyGO is installed as part of the [Hylleraas Software
Platform](<https://gitlab.com/hylleraasplatform/hylleraas>) or locally
via cloning the repository and installing with pip.
