import copy
from typing import Optional

import numpy as np
import pytest

import hygo

nh3 = """4
Energy =
N     0.0000000   -0.0412532   -0.2360743
H     0.8228799   -0.1997923    0.3677681
H    -0.8228799   -0.1997923    0.3677681
H    -0.0000000    0.9700151   -0.4465441"""


class AmmoniaInversion:
    """Class for performing geometry optimizations for ammonia in the inversion barrier."""

    def __init__(self, molecule_str: str, barrier: Optional[float] = None, minimum: Optional[float] = None):

        self.barrier = 0.01 if barrier is None else barrier
        self.d = 0.0 if minimum is None else minimum

        self.k = 8.0 * self.barrier / (self.d**2)
        # use hygo's molecule reader
        self.molecule = hygo.Molecule(molecule_str)
        self.symmetrize_coordinates()
        self.initial_molecule = copy.deepcopy(self.molecule)

        self.test_list = [1, 2, 3]
        self.print_level = 100

    def symmetrize_coordinates(self):
        """Symmetrize coordinates in C3V/D3V."""
        ind_n = self.molecule.atoms.index('N')
        new_coord = self.molecule.coordinates
        xn = new_coord[ind_n]
        new_coord = np.delete(new_coord, ind_n, 0)
        rh = 0.0
        for coord in new_coord:
            rh += np.linalg.norm(xn - coord) / 3.0

        u, v, w = new_coord
        n = np.cross(u - w, v - w)
        d = np.dot(n, w)
        norm = np.linalg.norm(n)
        dist = np.abs(np.dot(n, xn) - d) / norm
        self.molecule.atoms = ['N', 'H', 'H', 'H']
        self.molecule.coordinates[0] = np.zeros(3)
        c = np.cos(np.pi / 3.0) * rh
        s = np.sin(np.pi / 3.0) * rh
        self.molecule.coordinates[1] = np.array([0, rh, dist])
        self.molecule.coordinates[2] = np.array([s, -c, dist])
        self.molecule.coordinates[3] = np.array([-s, -c, dist])

    def update_coordinates(self, coordinates):
        """Update and scale coordinates."""
        self.molecule.coordinates = coordinates.ravel().reshape(-1, 3)
        return self.molecule

    def get_energy(self, molecule, test_list, test_dict=None):
        """Compute energy."""
        test_dict = {'key1': 'something'} if test_dict is None else test_dict
        x = molecule.coordinates[1][2]
        e = (x**2 - self.d**2)**2 * self.k / (8.0 * self.d**2)
        return e

    def get_gradient(self, molecule):
        """Compute energy."""
        x = molecule.coordinates[1][2]
        g = self.k / (2.0 * self.d**2) * x * (x**2 - self.d**2)
        gradient = np.zeros((self.molecule.num_atoms, 3))
        gradient[1] = np.array([0, 0, g])
        gradient[2] = np.array([0, 0, g])
        gradient[3] = np.array([0, 0, g])
        return gradient

    @staticmethod
    def angle_nhn(coordinates):
        """Compute angle N-H-N."""
        r = coordinates[1][1]
        d = coordinates[1][2]
        return np.arccos((-r**2 * np.cos(np.pi / 3.0) + d**2) / (r**2 + d**2)) * 180.0 / np.pi


@pytest.fixture(scope='session', params=[0.405, 0.415])
def gen_calc(request):
    """Generate AmmoniaInversion instance."""
    yield AmmoniaInversion(nh3, barrier=0.01, minimum=request.param)


def test_optimizer(gen_calc):
    """Test optimizers."""
    geo_options = {
        'optimizer': 'steepest_descent', 'convergence_energy': 1e-8, 'convergence_gradient_max': 5e-3,
        'convergence_gradient_rms': 5e-3
    }
    geo_options2 = {
        'optimizer': 'steepest_descent', 'convergence_energy': 1e-8, 'convergence_gradient_max': 5e-3,
        'convergence_gradient_rms': 5e-3, 'constraints': {'freeze': ['xyz', 1, 2]}
    }
    geo_options3 = {'optimizer': 'steepest_descent', 'convergence_energy': 1e-18, 'steps_max': 3}

    geoopt = hygo.geometry_optimization(molecule=gen_calc.initial_molecule, method=gen_calc, options=geo_options)
    final_minimum = geoopt['geometries'][-1][1][2]
    assert abs(gen_calc.d - final_minimum) < 1e-2
    geoopt = hygo.geometry_optimization(molecule=gen_calc.initial_molecule, method=gen_calc, options=geo_options2)
    final_minimum = geoopt['geometries'][-1][1][2]
    assert abs(gen_calc.d - final_minimum) < 1e-2

    geoopt2 = hygo.geometry_optimization(molecule=gen_calc.initial_molecule, method=gen_calc, options=geo_options3)
    assert geoopt2['is_converged'] is False


def test_optimizer_exception(gen_calc):
    """Test optimizer exceptions."""
    geo_options = {'optimizer': 'unknown'}

    with pytest.raises(Exception):
        hygo.geometry_optimization(molecule=gen_calc.initial_molecule, method=gen_calc, options=geo_options)
