# TODO: EXPAND with meaningful example

import numpy as np

from hygo.check import check_iteration
from hygo.options import set_options

# import pytest

niter = 20
enlist = np.linspace(0, -1, niter)

geolist = []
gradlist = []
for a in np.linspace(1, 1.5, niter + 1):
    geolist.append([[0, 0, -a], [0, 0, a]])
    gradlist.append([[0, 0, -a / 10], [0, 0, a / 10]])

options = set_options({})


def test_check():
    """Test check function."""
    summary = check_iteration(enlist, gradlist, geolist, options)
    assert not summary['is_stable']
