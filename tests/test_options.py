import pytest

from hygo.options import set_options

# from typings import Dict

opt1: dict = {}

opt5 = {'convergence_energy': '1e-5'}
opt6 = {'optimizer': 2}
opt7 = {'debug': 'str'}
opt8 = {'steps_max': 2.0}


@pytest.fixture(scope='session', params=[opt1])
def get_options(request):
    """Ficture returning options."""
    yield request.param


@pytest.fixture(scope='session', params=[opt5, opt6, opt7, opt8])
def get_options_exception(request):
    """Fixture returning options."""
    yield request.param


def test_options(get_options):
    """Test options."""
    set_options(get_options)


def test_options_exception(get_options_exception):
    """Test exceptions in options."""
    with pytest.raises(Exception):
        set_options(get_options_exception)
