import pytest

from hygo.method import Compute

my_method: dict = {}
dummystring = 'dummy'


class MyComputeClass:
    """Compute class dummy."""

    def __init__(self, name: str):
        self.name = name

    def get_energy(self, molecule):
        """Compute energy (dummy)."""
        self.energy = 0.0
        return 0.0

    @staticmethod
    def get_gradient(self, molecule):
        """Compute gradient (dummy)."""
        self.gradient = 0.0
        return 0.0

    def get_hessian(self, molecule):
        """Compute hessian (dummy)."""
        self.hessian = 0.0
        return 0.0


class MyComputeClass1:
    """Compute class dummy."""

    def __init__(self, name: str):
        self.name = name

    def get_energy(self, molecule):
        """Compute energy (dummy)."""
        self.energy = 0.0
        return 0.0


class MyComputeClass2:
    """Compute class dummy."""

    def __init__(self, name: str):
        self.name = name

    def get_energy(self, molecule):
        """Compute energy (dummy)."""
        self.energy = 0.0
        return 0.0

    def get_gradient(self, molecule):
        """Compute energy (dummy)."""
        self.gradient = 0.0
        return 0.0


class MyFailingComputeClass:
    """Compute class dummy for testing exceptions."""

    def __init__(self, name: str):
        self.name = name

    @staticmethod
    def compute_energy():
        """Compute energy (dummy)."""
        pass


@pytest.fixture(scope='session')
def get_compute_class():
    """Generate class for hygo.Compute."""
    mycalc = MyComputeClass('dummy')
    yield mycalc


@pytest.fixture(scope='session')
def get_compute_class1():
    """Generate class for hygo.Compute."""
    mycalc = MyComputeClass1('dummy')
    yield mycalc


@pytest.fixture(scope='session')
def get_compute_class2():
    """Generate class for hygo.Compute."""
    mycalc = MyComputeClass2('dummy')
    yield mycalc


@pytest.fixture(scope='session')
def get_failing_compute_class():
    """Generate class for hygo.Compute."""
    mycalc = MyFailingComputeClass('dummy')
    yield mycalc


# @pytest.fixture(scope='session', params=[
#     'london.out',
# ])
# def get_london_output(request):
#     """Fixture returning LONDON output file."""
#     yield os.path.join(pytest.FIXTURE_DIR, request.param)


def test_method_input(get_compute_class, get_compute_class2, monkeypatch):
    """Test method setter in hygo.Method class."""
    mymeth = Compute({'key1': 'something'})
    assert mymeth.compute_class is None
    mymeth = Compute(get_compute_class)
    assert mymeth.has_hessian is True
    mymeth = Compute(get_compute_class2)
    assert mymeth.has_hessian is False
    coordinates = [0, 0, 0]
    mydict = {'a': -1, 'b': 1}
    mylist = ['a', 'b', 'c']
    arglist = Compute.set_args(locals(), 'coordinates')
    assert arglist[0] == coordinates
    with pytest.raises(Exception):
        Compute.set_args(locals(), 'ciirdinates')

    arglist = Compute.set_args(locals(), None)
    assert arglist == []
    arglist = Compute.set_args(locals(), [])
    assert arglist == []
    arglist = Compute.set_args(locals(), mydict)
    assert arglist['a'] == '-1'
    arglist = Compute.set_args(locals(), mylist)
    assert len(arglist) == 3


#     mymeth = Compute(molecule=get_molecule, method={'program': 'dalton', 'basis': 'cc-pVDZ', 'method': 'HF'})
#     assert hasattr(mymeth.compute_class, 'properties') is True
#     mol_new = mymeth.compute_class.update_coordinates(mymeth.compute_class.molecule.coordinates)
#     assert mol_new.coordinates[0][0] == 0

#     # pretending dummy.x is in path
#     monkeypatch.setenv('PATH', 'tests/data/', prepend=os.pathsep)
#     mymeth = Compute(molecule=get_molecule,
#                      method={'program': 'london', 'executable': 'dummy.x', 'options': {'system': {}}})
#     assert hasattr(mymeth.compute_class, 'london_output_parser_gradient') is True
#     with pytest.raises(Exception):
#         mymeth.compute_class.set_london_options({'scf': {'spin-symmetry-constraint': 'Unrestricted Hartree-Fock'}})
#     mymeth.compute_class.options = mymeth.compute_class.set_london_options(
#         {'scf': {'spin-symmetry-constraint': 'Unrestricted Hartree-Fock'}, 'system': {}})
#     assert 'Unrestricted' in str(mymeth.compute_class.options)
#     gen_input = mymeth.compute_class.gen_london_input()

#     assert 'charge = 17.0' in gen_input
#     mymeth.compute_class.filename_out = get_london_output
#     energy = mymeth.compute_class.london_output_parser_energy()
#     assert abs(energy + 3.170523991602534) < 1e-6
#     mymeth.compute_class.num_atoms = 4
#     mymeth.compute_class.scale = 1.0
#     gradient = mymeth.compute_class.london_output_parser_gradient()
#     assert abs(gradient[0][0] - 0.197852) < 1e-6


def test_method_input_exception(get_failing_compute_class, get_compute_class1):
    """Test exceptions in hygo.Method class."""
    with pytest.raises(Exception):
        Compute('mymethod')
    with pytest.raises(Exception):
        Compute([])
    with pytest.raises(Exception):
        Compute(get_compute_class1)
    # with pytest.raises(Exception):
    #     Compute(molecule=get_molecule, method={'program': 'unknown'})


#     with pytest.raises(Exception):
#         Compute(molecule=get_molecule, method={'program': 'london'})
#     with pytest.raises(Exception):
#         Compute(molecule=get_molecule, method=get_failing_compute_class)
#     with pytest.raises(Exception):
#         Compute(molecule=get_molecule, method='fail')
#     with pytest.raises(Exception):
#         Compute(molecule=get_molecule, method={'program': 'london'})
#     with pytest.raises(Exception):
#         Compute(molecule=get_molecule, method={'program': 'london', 'executable': 'dummy.x'})
