import os

import numpy as np
import pytest

from hygo.molecule import Molecule

molecule_string_1 = """
   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""

molecule_string_2 = """3

   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""

molecule_string_3 = """3

   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0. 0.0503996795 1.0394417252
   H 0.0 0.0162646901 0.8394416921"""

molecule_string_4 = """4

   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""


@pytest.fixture(scope='session', params=[
    'water.xyz',
])
def get_mol_file(request):
    """Fixture returning molecule filename."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


@pytest.fixture(scope='session', params=[
    'dummy.txt',
])
def get_dummy_file(request):
    """Fixture returning name of dummy file."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


@pytest.fixture(scope='session', params=[molecule_string_1, molecule_string_2, molecule_string_3])
def get_mol_string(request):
    """Ficture returning molecule string."""
    yield request.param


def test_molecule_input(get_mol_file, get_mol_string):
    """Test reader in hygo.Molecule class."""
    print('checking : ' + get_mol_file)
    mymol = Molecule(get_mol_file)
    assert mymol.atoms == ['H', 'O', 'H']
    mymol = Molecule(mymol)
    assert mymol.num_atoms == 3
    mymol = Molecule(get_mol_string)
    assert mymol.num_atoms == 3
    mymol2 = Molecule(mymol)
    assert np.allclose(mymol2.coordinates, mymol.coordinates)


def test_molecule_input_exception(get_dummy_file):
    """Test Exception in reader in hygo.Molecule class."""
    with pytest.raises(Exception):
        Molecule('4')
    with pytest.raises(Exception):
        Molecule('H 0 0')
    with pytest.raises(Exception):
        Molecule(4)
    with pytest.raises(Exception):
        Molecule(molecule_string_4)
    with pytest.raises(Exception):
        Molecule(get_dummy_file)
