import daltonproject as dp
import numpy as np
import pytest
from hyif import LSDalton

import hygo

molecule_string = """
H     0.0000000   -0.0938484   -0.9489909
O     0.0000000    0.2184167   -0.0228320
H     0.0000000   -0.6245684    0.4718229"""

geo_options1 = {
    'optimizer': 'geometric',
    'print_level': 1,
    'optimization_target': 'minimum',
}

geo_options2 = {
    'optimizer': 'geometric', 'steps_max': 200, 'print_level': 1, 'optimization_target': 'minimum',
    'constraints': {'freeze': ['distance', 0, 2]}
}

geo_options3 = {
    'optimizer': 'geometric', 'steps_max': 200, 'print_level': 1, 'optimization_target': 'minimum',
    'constraints': {'freeze': ['angle', 0, 1, 2]}
}

geo_options4 = {
    'optimizer': 'geometric', 'steps_max': 200, 'print_level': 1, 'optimization_target': 'minimum',
    'constraints': {'set': ['distance', 0, 2, 0.8031502416887247]}
}


@pytest.fixture(scope='session', params=[geo_options1, geo_options2, geo_options3, geo_options4])
def gen_opt(request):
    """Yield options."""
    yield request.param


@pytest.fixture(scope='session')
def get_compute_class():
    """Generate class for hygo.Compute."""
    method = {'qcmethod': ['DFT', 'BP86'], 'basis': 'cc-pVDZ'}
    mycalc = LSDalton(method)
    yield mycalc


@pytest.fixture(scope='session')
def gen_molecule():
    """Generate molecule for hygo.Compute."""
    mymol = dp.Molecule(atoms=molecule_string)
    yield mymol


def test_geometric(gen_opt, get_compute_class, gen_molecule):
    """Test geometric optimizer."""
    geo = hygo.geometry_optimization(molecule=gen_molecule, method=get_compute_class, options=gen_opt)
    energy = geo['energies'][-1]
    assert abs(76.421233761403 + energy) <= 1e-6


def test_geometric_scan(get_compute_class, gen_molecule):
    """Test geometric optimizer."""
    geo_options5 = {
        'optimizer': 'geometric', 'steps_max': 200, 'print_level': 1, 'optimization_target': 'minimum',
        'constraints': {'scan': ['distance', 0, 2, 0.802, 0.804, 3]}
    }
    geo = hygo.geometry_optimization(molecule=gen_molecule, method=get_compute_class, options=geo_options5)
    last_dist = np.linalg.norm(geo['geometries'][-1][2] - geo['geometries'][-1][0])
    assert abs(last_dist - 0.804) <= 1e-2
